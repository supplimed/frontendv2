module.exports = (req, res, next) => {

  if (req.method == 'POST' && req.path == '/authentication/login') {
    if (req.body.email === 'gogeta@dbz.com' && req.body.password === 'supersayan') {
      res.status(200).json({
        "user": {
          "id": "42",
          "email": "gogeta@dbz.com",
          "firstName": "gogeta",
          "lastName": "namek"
        },
        "token": "$2b$10$cx3cXA7Tg/phv3lCvrJ13.QBb3z9Z3EBkcxIFDazMq/67aaeq6Qwy"
      })
    } else {
      res.status(400).json({message: 'wrong password'})
    }
  } else {
    next()
  }
}
