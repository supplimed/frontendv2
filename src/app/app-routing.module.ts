import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { HomePageComponent } from './containers/home-page/home-page.component'
import { BasketPageComponent } from './containers/basket-page/basket-page.component'
import { SignUpPageComponent } from './containers/sign-up-page/sign-up-page.component'
import { LogInPageComponent } from './containers/log-in-page/log-in-page.component'
import { ProductsPageComponent } from './containers/products-page/products-page.component'
import { ProductFocusPageComponent } from './containers/product-focus-page/product-focus-page.component'
import { ProfilePageComponent } from './containers/account-page/profile-page/profile-page.component'
import { AuthGuard } from './shared/core/auth.guard'
import { FormProfilePageComponent } from './containers/basket-page/form-profile-page/form-profile-page.component'
import { PaymentPageComponent } from './containers/basket-page/payment-page/payment-page.component'
import { PaymentAcceptedPageComponent } from './containers/payment-accepted-page/payment-accepted-page.component'
import { DeliveryPageComponent } from './containers/basket-page/delivery-page/delivery-page.component'
import { BasketSummaryComponent } from './containers/basket-page/basket-summary-page/basket-summary.component'
import { OrderHistoryPageComponent } from './containers/account-page/order-history-page/order-history-page.component'
import { OrderHistoryFocusPageComponent } from './containers/order-history-focus-page/order-history-focus-page.component'
import { AccountPageComponent } from './containers/account-page/account-page.component'

const routes: Routes = [
  { path: 'home', component: HomePageComponent },
  { path: '', component: HomePageComponent },
  { path: 'products', component: ProductsPageComponent },
  { path: 'products/:id', component: ProductFocusPageComponent },
  { path: 'signup', component: SignUpPageComponent },
  { path: 'login', component: LogInPageComponent },
  { path: 'payment/accepted', component: PaymentAcceptedPageComponent, canActivate: [AuthGuard] },
  { path: 'order/history/:id', component: OrderHistoryFocusPageComponent },
  {
    path: 'basket',
    component: BasketPageComponent,
    children: [
      { path: 'summary', component: BasketSummaryComponent },
      { path: 'delivery', component: DeliveryPageComponent },
      { path: 'profile', component: FormProfilePageComponent, canActivate: [AuthGuard] },
      { path: 'payment', component: PaymentPageComponent, canActivate: [AuthGuard] }
    ]
  },
  {
    path: 'account',
    component: AccountPageComponent,
    children: [
      { path: 'profile', component: ProfilePageComponent, canActivate: [AuthGuard] },
      { path: 'order/history', component: OrderHistoryPageComponent }
    ]
  }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
