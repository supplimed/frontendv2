import { BrowserModule } from '@angular/platform-browser'
import { LOCALE_ID, NgModule } from '@angular/core'
import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http'
import { ReactiveFormsModule } from '@angular/forms'
import { ComponentsModule } from './shared/components'
import { ContainersModule } from './containers'
import { JwtInterceptor } from './shared/core/jwt.interceptor'
import { MatMomentDateModule } from '@angular/material-moment-adapter'
import { MAT_DATE_LOCALE } from '@angular/material/core'
import { OrderSummaryInfoComponent } from './shared/components/order-summary-info/order-summary-info.component'

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    ComponentsModule,
    ContainersModule,
    MatMomentDateModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: MAT_DATE_LOCALE, useValue: 'fr-FR' },
    { provide: LOCALE_ID, useValue: 'fr' }
  ],
  exports: [
    OrderSummaryInfoComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
