import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { OrderHistoryPageComponent } from '../account-page/order-history-page/order-history-page.component'

describe('OrderHistoryPageComponent', () => {
  let component: OrderHistoryPageComponent
  let fixture: ComponentFixture<OrderHistoryPageComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        OrderHistoryPageComponent
      ],
      imports: [
        HttpClientTestingModule
      ]
    })
      .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderHistoryPageComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
