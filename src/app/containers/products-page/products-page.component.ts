import { Component, OnInit } from '@angular/core'
import { ProductService } from 'src/app/shared/services/product.service'
import { PaginationI, ProductData } from '../../shared/models/product.data'

@Component({
  selector: 'app-products-page',
  templateUrl: './products-page.component.html',
  styleUrls: ['./products-page.component.scss']
})
export class ProductsPageComponent implements OnInit {
  public products: ProductData[]
  public pagination: PaginationI

  constructor (public productService: ProductService) {
  }

  ngOnInit (): void {
    this.productService.getProducts().subscribe()
    this.productService.products$.subscribe(
      data => {
        this.products = data.data
        this.pagination = data.pagination
      })
  }
}
