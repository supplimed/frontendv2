import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { ProductsPageComponent } from './products-page.component'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { CURRENCIES, ProductData } from '../../shared/models/product.data'
import { ProductCardComponent } from '../../shared/components/product-card/product-card.component'
import { RouterTestingModule } from '@angular/router/testing'
import { PaginatorComponent } from '../../shared/components/paginator/paginator.component'

const mockProducts: ProductData[] = [
  {
    id: '0',
    name: 'Name for 0',
    price: 0,
    currency: CURRENCIES.EURO,
    description: 'Description for 0',
    imageUrl: 'assets/images/potaras.png',
    type: 'product'
  },
  {
    id: '1',
    name: 'Name for 1',
    price: 1,
    currency: CURRENCIES.EURO,
    description: 'Description for 1',
    imageUrl: 'assets/images/potaras.png',
    type: 'product'
  }
]

describe('ProductsComponent', () => {
  let component: ProductsPageComponent
  let fixture: ComponentFixture<ProductsPageComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ProductsPageComponent,
        ProductCardComponent,
        PaginatorComponent
      ],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule
      ]
    })
      .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsPageComponent)
    component = fixture.componentInstance
    component.products = mockProducts
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
