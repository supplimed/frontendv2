import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { ProductFocusPageComponent } from './product-focus-page.component'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { ActivatedRoute } from '@angular/router'
import { CURRENCIES, ProductData } from '../../shared/models/product.data'

describe('ProductFocusPageComponent', () => {
  let component: ProductFocusPageComponent
  let fixture: ComponentFixture<ProductFocusPageComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProductFocusPageComponent],
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: { get: (): number => 1 }
            }
          }
        }]
    })
      .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductFocusPageComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
    component.product = new ProductData({
      id: '1',
      name: 'product1',
      price: 2,
      currency: CURRENCIES.EURO,
      description: '',
      imageUrl: '',
      type: ''
    })
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('should add product', () => {
    component.quantity = 1
    component.addProduct()
    expect(component.quantity).toEqual(2)
    expect(component.totalPrice).toEqual(4)
  })

  it('should remove a product', () => {
    component.quantity = 2
    component.removeProduct()
    expect(component.quantity).toEqual(1)
    expect(component.totalPrice).toEqual(2)
  })

  it('should not remove a product if the quantity is already 1', () => {
    component.quantity = 1
    component.totalPrice = 2
    component.removeProduct()
    expect(component.quantity).toEqual(1)
    expect(component.totalPrice).toEqual(2)
  })
})
