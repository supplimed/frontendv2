import { Component, OnInit } from '@angular/core'
import { ProductService } from '../../shared/services/product.service'
import { ActivatedRoute } from '@angular/router'
import { ProductData } from '../../shared/models/product.data'
import { BasketService } from '../../shared/services/basket.service'

@Component({
  selector: 'app-product-focus-page',
  templateUrl: './product-focus-page.component.html',
  styleUrls: ['./product-focus-page.component.scss']
})
export class ProductFocusPageComponent implements OnInit {
  public product: ProductData
  public productNotFound: boolean
  public totalPrice: number
  public quantity: number

  constructor (private productService: ProductService,
              private basketService: BasketService,
              private route: ActivatedRoute) { }

  ngOnInit (): void {
    this.productNotFound = false
    const id = +this.route.snapshot.paramMap.get('id')
    this.productService.getProductById(id).subscribe(
      data => {
        this.product = data.data
        this.totalPrice = this.product.price
      },
      error => {
        console.log(error) // TODO need to handle error
        this.productNotFound = true
      })

    this.quantity = 1
  }

  addProduct (): void {
    this.quantity++
    this.totalPrice = this.quantity * this.product.price
  }

  removeProduct (): void {
    if (this.quantity > 1) {
      this.quantity -= 1
      this.totalPrice = this.quantity * this.product.price
    }
  }

  addToBasket (): void {
    const products = [{
      id: this.product.id,
      quantity: this.quantity
    }]

    this.basketService.addBasket(products).subscribe()
  }
}
