import { Component, OnInit } from '@angular/core'
import { OrderHistoryI } from '../../shared/models/orderHistory.data'
import { ActivatedRoute } from '@angular/router'
import { BasketService } from '../../shared/services/basket.service'

@Component({
  selector: 'app-order-history-focus-page',
  templateUrl: './order-history-focus-page.component.html',
  styleUrls: ['./order-history-focus-page.component.scss']
})
export class OrderHistoryFocusPageComponent implements OnInit {
  orderHistoryNotFound: boolean
  orderHistory: OrderHistoryI

  constructor (private route: ActivatedRoute,
               private basketService: BasketService) {
  }

  ngOnInit (): void {
    this.orderHistoryNotFound = false
    const id = this.route.snapshot.paramMap.get('id')
    this.basketService.getOrderHistoryById(id).subscribe(
      data => {
        this.orderHistory = data
      },
      error => {
        console.log(error)
        this.orderHistoryNotFound = true
      })
  }
}
