import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { OrderHistoryFocusPageComponent } from './order-history-focus-page.component'
import { ActivatedRoute } from '@angular/router'
import { HttpClientTestingModule } from '@angular/common/http/testing'

describe('OrderHistoryFocusPageComponent', () => {
  let component: OrderHistoryFocusPageComponent
  let fixture: ComponentFixture<OrderHistoryFocusPageComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      declarations: [OrderHistoryFocusPageComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: { get: (): number => 1 }
            }
          }
        }]
    })
      .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderHistoryFocusPageComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
