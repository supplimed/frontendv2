import { NgModule } from '@angular/core'
import { BasketPageComponent } from './basket-page/basket-page.component'
import { HomePageComponent } from './home-page/home-page.component'
import { ProductsPageComponent } from './products-page/products-page.component'
import { LogInPageComponent } from './log-in-page/log-in-page.component'
import { SignUpPageComponent } from './sign-up-page/sign-up-page.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { BrowserModule } from '@angular/platform-browser'
import { ComponentsModule } from '../shared/components'
import { RouterModule } from '@angular/router'
import { ProfilePageComponent } from './account-page/profile-page/profile-page.component'
import { ProductFocusPageComponent } from './product-focus-page/product-focus-page.component'
import { MatButtonModule } from '@angular/material/button'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { MatRippleModule } from '@angular/material/core'
import { MatIconModule } from '@angular/material/icon'
import { AppRoutingModule } from '../app-routing.module'
import { HttpClientModule } from '@angular/common/http'
import { MatFormFieldModule } from '@angular/material/form-field'
import { MatInputModule } from '@angular/material/input'
import { FormProfilePageComponent } from './basket-page/form-profile-page/form-profile-page.component'
import { PaymentPageComponent } from './basket-page/payment-page/payment-page.component'
import { CommonModule } from '@angular/common'
import { MatSelectModule } from '@angular/material/select'
import { MatDatepickerModule } from '@angular/material/datepicker'
import { MatCheckboxModule } from '@angular/material/checkbox'
import { PaymentAcceptedPageComponent } from './payment-accepted-page/payment-accepted-page.component'
import { ScrollingModule } from '@angular/cdk/scrolling'
import { DeliveryPageComponent } from './basket-page/delivery-page/delivery-page.component'
import { MatStepperModule } from '@angular/material/stepper'
import { BasketSummaryComponent } from './basket-page/basket-summary-page/basket-summary.component'
import { OrderHistoryFocusPageComponent } from './order-history-focus-page/order-history-focus-page.component'
import { OrderHistoryPageComponent } from './account-page/order-history-page/order-history-page.component'
import { AccountPageComponent } from './account-page/account-page.component'
import { MatDialogModule } from '@angular/material/dialog'

@NgModule({
  declarations: [
    BasketPageComponent,
    HomePageComponent,
    ProductsPageComponent,
    LogInPageComponent,
    SignUpPageComponent,
    ProfilePageComponent,
    ProductFocusPageComponent,
    FormProfilePageComponent,
    PaymentPageComponent,
    PaymentAcceptedPageComponent,
    DeliveryPageComponent,
    BasketSummaryComponent,
    OrderHistoryPageComponent,
    AccountPageComponent,
    OrderHistoryFocusPageComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    ComponentsModule,
    RouterModule,

    MatDatepickerModule,
    MatSelectModule,
    MatIconModule,
    MatButtonModule,
    MatRippleModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    AppRoutingModule,
    HttpClientModule,
    MatCheckboxModule,
    ScrollingModule,
    MatDialogModule,
    CommonModule,
    MatStepperModule
  ],
  exports: [
    BasketPageComponent,
    HomePageComponent,
    ProductsPageComponent,
    LogInPageComponent,
    SignUpPageComponent,
    ProfilePageComponent,
    ProductFocusPageComponent,
    FormProfilePageComponent,
    PaymentPageComponent,
    PaymentAcceptedPageComponent,
    DeliveryPageComponent,
    BasketSummaryComponent,
    OrderHistoryPageComponent,
    AccountPageComponent,
    OrderHistoryFocusPageComponent
  ]
})
export class ContainersModule { }
