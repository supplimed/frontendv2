import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { PaymentService } from '../../../shared/services/payment.service'
import { MangoPayRegisteredCardI, MangoPayCardI, MangoPayRegistrationCardI, MangoPayCardFormI } from '../../../shared/models/mangoPay.data'
import { Router } from '@angular/router'
import { BasketService } from '../../../shared/services/basket.service'
import { StepperService } from '../stepper.service'
import { AddressI } from '../../../shared/models/address.data'
import { BasketI } from '../../../shared/models/basket.data'
import { MatDialog } from '@angular/material/dialog'
import { CreditCardSelectionComponent } from '../../../shared/components/credit-card-selection/credit-card-selection.component'

const EXPIRATION_YEARS = 20

interface Month {
  value: string
}

interface Year {
  value: string
}

@Component({
  selector: 'app-payment-page',
  templateUrl: './payment-page.component.html',
  styleUrls: ['./payment-page.component.scss']
})
export class PaymentPageComponent implements OnInit {
  cardForm: FormGroup
  cards: MangoPayRegisteredCardI[]
  selectedCardValue: MangoPayRegisteredCardI
  selectedCard: number
  selectedCardInfo: string
  deliveryAddress: AddressI
  billingAddress: AddressI
  basket: BasketI

  months: Month[] = [
    { value : '01' },
    { value : '02' },
    { value : '03' },
    { value : '04' },
    { value : '05' },
    { value : '06' },
    { value : '07' },
    { value : '08' },
    { value : '09' },
    { value : '10' },
    { value : '11' },
    { value : '12' }
  ]

  years: Year[] = [];

  constructor (private formBuilder: FormBuilder,
               private paymentService: PaymentService,
               private basketService: BasketService,
               private stepperService: StepperService,
               private router: Router,
               private dialog: MatDialog) {
    this.stepperService.setStep(3)
  }

  ngOnInit (): void {
    this.cardForm = this.formBuilder.group({
      cardNumber: ['', [Validators.required]],
      cardExpirationMonthDate: ['', [Validators.required]],
      cardExpirationYearDate: ['', [Validators.required]],
      cardCvx: ['', [Validators.required]]
    })

    // Creation of years
    let currentYear: number = new Date().getFullYear()
    currentYear = currentYear - 2000
    for (let i = 0; i <= EXPIRATION_YEARS; i++) {
      const temp = currentYear + i
      this.years.push({ value: temp.toString() })
    }
    this.paymentService.getUserCards().subscribe((data) => {
      this.cards = data.cards
      this.selectedCardInfo = this.cards[0].id
      this.selectedCardValue = this.cards[0]
    })

    this.basketService.getOrder().subscribe((data) => {
      this.deliveryAddress = data.deliveryAddress
      this.billingAddress = data.billingAddress
    })
    this.selectedCard = 0
    this.basketService.getBasket().subscribe(data => { this.basket = data })
  }

  onSubmit (value: MangoPayCardFormI): void {
    this.paymentService.createCardRegistration().subscribe(data => {
      const cardRegistrationInit: MangoPayRegistrationCardI = {
        accessKey: data.cardRegistrationData.AccessKey,
        preregistrationData: data.cardRegistrationData.PreregistrationData,
        cardRegistrationURL: data.cardRegistrationData.CardRegistrationURL,
        Id: data.cardRegistrationData.Id
      }

      const cardData: MangoPayCardI = {
        cardNumber: value.cardNumber,
        cardExpirationDate: value.cardExpirationMonthDate + '/' + value.cardExpirationYearDate,
        cardCvx: value.cardCvx,
        cardType: 'CB_VISA_MASTERCARD'
      }

      this.paymentService.sendCardDetailForTokenization(cardRegistrationInit, cardData)
    })
  }

  pay (): void {
    this.paymentService.directCardPayIn(this.selectedCardInfo).subscribe((res) => {
      if (res.mpStatus === 'SUCCEEDED') {
        this.router.navigate(['/payment/accepted'])
      }
    })
  }

  setSelectCard (id: number, cardId: string): void {
    this.selectedCard = id
    this.selectedCardInfo = cardId
  }

  openDialog (): void {
    const dialogRef = this.dialog.open(CreditCardSelectionComponent, {
      data: {
        cards: this.cards,
        selectedCard: this.selectedCard
      },
      width: '600px',
      height: '400px'
    })

    dialogRef.afterClosed().subscribe(result => {
      if (!result) return

      if (result.event === 'select') {
        this.selectedCard = result.selectedCard
        this.selectedCardValue = result.selectedCardValue
      } else if (result.event === 'add') {
        this.onSubmit(result.newCard)
      }
    })
  }
}
