import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { PaymentPageComponent } from './payment-page.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { MatSelectModule } from '@angular/material/select'
import { MatFormFieldModule } from '@angular/material/form-field'
import { MatInputModule } from '@angular/material/input'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { MatIcon, MatIconModule } from '@angular/material/icon'
import { ScrollingModule } from '@angular/cdk/scrolling'
import { OrderAddressesInfoComponent } from '../../../shared/components/order-addresses-info/order-addresses-info.component'
import { AddressElementComponent } from '../../../shared/components/address-element/address-element.component'
import { MatDialogModule } from '@angular/material/dialog'
import { MatIconTestingModule } from '@angular/material/icon/testing'

describe('PaymentPageComponent', () => {
  let component: PaymentPageComponent
  let fixture: ComponentFixture<PaymentPageComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        PaymentPageComponent,
        OrderAddressesInfoComponent,
        AddressElementComponent,
        MatIcon
      ],
      imports: [
        ReactiveFormsModule,
        FormsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        BrowserAnimationsModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatIconModule,
        ScrollingModule,
        MatDialogModule,
        MatIconTestingModule
      ]
    })
      .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentPageComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
