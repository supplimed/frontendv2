import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { BasketSummaryComponent } from './basket-summary.component'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { RouterTestingModule } from '@angular/router/testing'

describe('BasketSummaryComponent', () => {
  let component: BasketSummaryComponent
  let fixture: ComponentFixture<BasketSummaryComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BasketSummaryComponent],
      imports: [HttpClientTestingModule, RouterTestingModule]
    })
      .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(BasketSummaryComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
