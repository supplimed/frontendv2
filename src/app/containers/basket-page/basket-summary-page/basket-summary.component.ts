import { Component, OnInit } from '@angular/core'
import { BasketI } from '../../../shared/models/basket.data'
import { BasketService } from '../../../shared/services/basket.service'
import { AccountService } from '../../../shared/services/account.service'
import { StepperService } from '../stepper.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-basket-summary',
  templateUrl: './basket-summary.component.html',
  styleUrls: ['./basket-summary.component.scss']
})
export class BasketSummaryComponent implements OnInit {
  public basket: BasketI

  constructor (private basketService: BasketService,
               private accountService: AccountService,
               public stepperService: StepperService,
               public router: Router) {
    this.stepperService.setStep(0)
  }

  ngOnInit (): void {
    this.basketService.getBasket().subscribe()
    this.basketService.basket$.subscribe(data => { this.basket = data })
  }

  onNavigate (): void {
    if (this.accountService.getCurrentUser().mpClientId === null) {
      this.router.navigate(['/basket/profile'])
    } else {
      this.router.navigate(['/basket/delivery'])
    }
  }
}
