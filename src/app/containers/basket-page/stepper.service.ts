import { Injectable } from '@angular/core'

@Injectable({
  providedIn: 'root'
})
export class StepperService {
  stepNumber: number
  index: number
  completedTab: boolean[]

  constructor () {
    this.stepNumber = 3
    this.index = 0
    this.completedTab = new Array(3).fill(false)
  }

  public setStep (index: number): void {
    // To process short stepper
    if (index > 1 && this.stepNumber === 3) index -= 1

    this.index = index

    for (let i = 0; i < index; i++) {
      this.completedTab[i] = true
    }

    for (let i = index; i < this.stepNumber; i++) {
      this.completedTab[i] = false
    }
  }

  public setStepNumber (stepNumber: number): void {
    this.stepNumber = stepNumber
    if (stepNumber === 4 && this.completedTab.length === 3) this.completedTab.push(false)
    if (stepNumber === 3 && this.completedTab.length === 4) this.completedTab.pop()
  }

  public reset (): void{
    this.index = 0
    if (this.stepNumber === 4) this.completedTab = [false, false, false, false]
    if (this.stepNumber === 3) this.completedTab = [false, false, false]
  }
}
