import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { FormProfilePageComponent } from './form-profile-page.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { MatFormFieldModule } from '@angular/material/form-field'
import { MatInputModule } from '@angular/material/input'
import { MatSelectModule } from '@angular/material/select'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { MatDatepickerModule } from '@angular/material/datepicker'
import { MatMomentDateModule } from '@angular/material-moment-adapter'
import { RouterTestingModule } from '@angular/router/testing'

describe('FormProfilePageComponent', () => {
  let component: FormProfilePageComponent
  let fixture: ComponentFixture<FormProfilePageComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FormProfilePageComponent],
      imports: [
        ReactiveFormsModule,
        FormsModule,
        MatDatepickerModule,
        HttpClientTestingModule,
        BrowserAnimationsModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatMomentDateModule,
        RouterTestingModule
      ]
    })
      .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(FormProfilePageComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
