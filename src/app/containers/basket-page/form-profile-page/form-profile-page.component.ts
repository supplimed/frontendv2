import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { PaymentService } from '../../../shared/services/payment.service'
import { MangoPayLegalUserI } from '../../../shared/models/mangoPay.data'
import { DateAdapter } from '@angular/material/core'
import { StepperService } from '../stepper.service'
import { Router } from '@angular/router'
// import * as moment from 'moment'
// import * as momentTZ from 'moment-timezone'

// const MINUTE_TO_SECONDS = 60

interface LegalPersonType {
  type: string
  viewValue: string
}

interface CountryIso {
  value: string
  viewValue: string
}

@Component({
  selector: 'app-form-profile-page',
  templateUrl: './form-profile-page.component.html',
  styleUrls: ['./form-profile-page.component.scss']
})

export class FormProfilePageComponent implements OnInit {
  userInformationForm: FormGroup

legalPersonTypes: LegalPersonType[] = [
  { type: 'BUSINESS', viewValue: 'Business' },
  { type: 'ORGANIZATION', viewValue: 'Organization' },
  { type: 'SOLETRADER', viewValue: 'Soletrader' }
];

countryIsos: CountryIso[] = [
  { value: 'BE', viewValue: 'Belgique' },
  { value: 'FR', viewValue: 'France' },
  { value: 'LU', viewValue: 'Luxembourg' },
  { value: 'MC', viewValue: 'Monaco' },
  { value: 'GB', viewValue: 'Royaume-Uni' }
];

constructor (private formBuilder: FormBuilder,
             private paymentService: PaymentService,
             private stepperService: StepperService,
             private router: Router,
             private _adapter: DateAdapter<any>) {
  this._adapter.setLocale('fr')
  this.stepperService.setStep(1)
}

ngOnInit (): void {
  this.userInformationForm = this.formBuilder.group({
    LegalPersonType: ['', [Validators.required]],
    Name: ['', [Validators.required]],
    LegalRepresentativeBirthday: ['', [Validators.required]],
    LegalRepresentativeCountryOfResidence: ['', [Validators.required]],
    LegalRepresentativeNationality: ['', [Validators.required]],
    LegalRepresentativeFirstName: ['', [Validators.required]],
    LegalRepresentativeLastName: ['', [Validators.required]],
    Email: ['', [Validators.required]]
  })
}

onSubmit ({ value }: {value: MangoPayLegalUserI}): void {
  // Get the offset to keep the right date (day, month, year)
/*  const offset = momentTZ.tz('Europe/Paris').utcOffset()

  // Add the offset to the UTC date
  value.LegalRepresentativeBirthday = moment(value.LegalRepresentativeBirthday).unix() + offset * MINUTE_TO_SECONDS */
  this.paymentService.addCostumer(value).subscribe(data => { if (data) { this.router.navigate(['/basket/delivery']) } })
}
}
