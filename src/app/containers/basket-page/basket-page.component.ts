import { Component } from '@angular/core'
import { StepperService } from './stepper.service'
import { AccountService } from '../../shared/services/account.service'

@Component({
  selector: 'app-basket-page',
  templateUrl: './basket-page.component.html',
  styleUrls: ['./basket-page.component.scss']
})
export class BasketPageComponent {
  showProfileStep: boolean

  constructor (public stepperService: StepperService,
               public accountService: AccountService) {
    this.stepperService.reset()
    if (this.accountService.getCurrentUser().mpClientId === null) {
      this.showProfileStep = true
      this.stepperService.setStepNumber(4)
    } else {
      this.showProfileStep = false
      this.stepperService.setStepNumber(3)
    }
  }
}
