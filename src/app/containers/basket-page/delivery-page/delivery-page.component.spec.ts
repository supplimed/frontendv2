import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { DeliveryPageComponent } from './delivery-page.component'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { MatCheckboxModule } from '@angular/material/checkbox'
import { AddressI } from '../../../shared/models/address.data'
import { MatDialogModule } from '@angular/material/dialog'
import { AddressElementComponent } from '../../../shared/components/address-element/address-element.component'

describe('DeliveryPageComponent', () => {
  let component: DeliveryPageComponent
  let fixture: ComponentFixture<DeliveryPageComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        DeliveryPageComponent,
        AddressElementComponent
      ],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        MatCheckboxModule,
        MatDialogModule
      ]
    })
      .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveryPageComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('should invert the value of showBillingAddress', () => {
    component.showBillingAddress = true
    component.setShowBillingAddress()
    expect(component.showBillingAddress).toEqual(false)
  })

  it('should set the delivery address', () => {
    const targetAddress: AddressI = new class implements AddressI {
      address: 'testAddress';
      city: 'testCity';
      postalCode: 'testPostalCode';
    }()
    component.setSelectedDeliveryAddress(5, targetAddress)
    expect(component.selectedDeliveryAddress).toEqual(5)
    expect(component.selectedDeliveryAddressValue).toEqual(targetAddress)
  })

  it('should set the billing address', () => {
    const targetAddress: AddressI = new class implements AddressI {
      address: 'testAddress';
      city: 'testCity';
      postalCode: 'testPostalCode';
    }()
    component.setSelectedBillingAddress(5, targetAddress)
    expect(component.selectedBillingAddress).toEqual(5)
    expect(component.selectedBillingAddressValue).toEqual(targetAddress)
  })
})
