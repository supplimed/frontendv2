import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { AddressI } from '../../../shared/models/address.data'
import { AccountService } from '../../../shared/services/account.service'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { UserService } from '../../../shared/services/user.service'
import { BasketService } from '../../../shared/services/basket.service'
import { StepperService } from '../stepper.service'
import { BasketI } from '../../../shared/models/basket.data'
import { MatDialog } from '@angular/material/dialog'
import { AddressSelectionComponent } from '../../../shared/components/address-selection/address-selection.component'

enum AddressType {
  delivery,
  billing
}

@Component({
  selector: 'app-delivery-page',
  templateUrl: './delivery-page.component.html',
  styleUrls: ['./delivery-page.component.scss']
})
export class DeliveryPageComponent implements OnInit {
  addresses: AddressI[]

  selectedDeliveryAddress: number
  selectedDeliveryAddressValue: AddressI

  selectedBillingAddress: number
  selectedBillingAddressValue: AddressI

  deliveryAddressForm: FormGroup
  showBillingAddress: boolean
  addingAddressSectionTitle: string
  showAddingAddressSection: boolean
  addressType: AddressType
  addressTypeEnum: typeof AddressType = AddressType
  basket: BasketI

  constructor (private accountService: AccountService,
               private router: Router,
               private userService: UserService,
               private basketService: BasketService,
               private stepperService: StepperService,
               private formBuilder: FormBuilder,
               public dialog: MatDialog) {
    this.stepperService.setStep(2)
  }

  ngOnInit (): void {
    this.selectedDeliveryAddress = 0
    this.selectedBillingAddress = 0
    this.showAddingAddressSection = false
    this.showBillingAddress = false
    this.basketService.getBasket().subscribe(data => { this.basket = data })

    this.userService.getUser().subscribe((data) => {
      this.addresses = data.addresses
      this.selectedDeliveryAddressValue = this.addresses[0]
      this.selectedBillingAddressValue = this.selectedDeliveryAddressValue
    })

    this.deliveryAddressForm = this.formBuilder.group({
      address: ['', [Validators.required]],
      postalCode: ['', [Validators.required]],
      city: ['', [Validators.required]]
    })
  }

  setShowBillingAddress (): void{
    this.showBillingAddress = !this.showBillingAddress
  }

  setSelectedDeliveryAddress (index: number, address: AddressI): void {
    this.selectedDeliveryAddress = index
    this.selectedDeliveryAddressValue = address
  }

  setSelectedBillingAddress (index: number, address: AddressI): void {
    this.selectedBillingAddress = index
    this.selectedBillingAddressValue = address
  }

  onAddAddress (value: AddressI): void {
    this.userService.addUserAddress(value).subscribe((data) => {
      this.addresses = data.addresses
    })
  }

  sendAddress (): void {
    let billingAddressTemp: AddressI
    this.showBillingAddress ? billingAddressTemp = this.selectedBillingAddressValue : billingAddressTemp = this.selectedDeliveryAddressValue
    this.basketService.addOrderAddress(this.selectedDeliveryAddressValue, billingAddressTemp).subscribe()
    this.router.navigate(['/basket/payment'])
    this.stepperService.setStep(2)
  }

  openDialog (type: AddressType): void {
    const dialogRef = this.dialog.open(AddressSelectionComponent, {
      data: {
        addresses: this.addresses,
        selectedAddress: this.selectedDeliveryAddress,
        type: type
      },
      height: '400px',
      width: '600px'
    })

    dialogRef.afterClosed().subscribe(result => {
      if (!result) return

      if (result.event === 'select') {
        this.selectedDeliveryAddress = result.selectedAddress
        result.type === AddressType.delivery ? this.selectedDeliveryAddressValue = result.address : this.selectedBillingAddressValue = result.address
      } else if (result.event === 'add') {
        this.onAddAddress(result.newAddress)
      }
    })
  }
}
