import { TestBed } from '@angular/core/testing'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { StepperService } from './stepper.service'

describe('StepperService', () => {
  let service: StepperService

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    })
    service = TestBed.inject(StepperService)
  })

  it('should be created', () => {
    expect(service).toBeTruthy()
  })

  it('should set index and completedTab to the right value after setStep with a stepNumber of 4', () => {
    service.stepNumber = 4
    service.setStep(2)
    expect(service.index).toEqual(2)
    expect(service.completedTab).toEqual([true, true, false, false])
  })

  it('should set index and completedTab to the right value after setStep with a stepNumber of 3', () => {
    service.stepNumber = 3
    service.setStep(2)
    expect(service.index).toEqual(1)
    expect(service.completedTab).toEqual([true, false, false])
  })

  it('should reset index and completedTab (stepNumber of 3)', () => {
    service.stepNumber = 3
    service.reset()
    expect(service.index).toEqual(0)
    expect(service.completedTab).toEqual([false, false, false])
  })

  it('should reset index and completedTab (stepNumber of 4)', () => {
    service.stepNumber = 4
    service.reset()
    expect(service.index).toEqual(0)
    expect(service.completedTab).toEqual([false, false, false, false])
  })

  it('should set stepNumber to 3', () => {
    service.stepNumber = 4
    service.setStepNumber(3)
    expect(service.stepNumber).toEqual(3)
    expect(service.completedTab).toEqual([false, false, false])
  })

  it('should set stepNumber to 3 and reduce the size of completedTab', () => {
    service.stepNumber = 4
    service.completedTab = [false, false, false, false]
    service.setStepNumber(3)
    expect(service.stepNumber).toEqual(3)
    expect(service.completedTab).toEqual([false, false, false])
  })

  it('should set stepNumber to 4 and increase the size of completedTab', () => {
    service.stepNumber = 3
    service.completedTab = [false, false, false]
    service.setStepNumber(4)
    expect(service.stepNumber).toEqual(4)
    expect(service.completedTab).toEqual([false, false, false, false])
  })
})
