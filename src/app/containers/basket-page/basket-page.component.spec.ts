import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { BasketPageComponent } from './basket-page.component'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { AccountService } from '../../shared/services/account.service'
import { MatStepperModule } from '@angular/material/stepper'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { RouterTestingModule } from '@angular/router/testing'

export class AccountServiceStub {
  getCurrentUser (): { mpCardId: string } {
    return { mpCardId: '123' }
  }
}

describe('BasketPageComponent', () => {
  let component: BasketPageComponent
  let fixture: ComponentFixture<BasketPageComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BasketPageComponent],
      imports: [
        HttpClientTestingModule,
        MatStepperModule,
        BrowserAnimationsModule,
        RouterTestingModule
      ]
    })
      .overrideComponent(BasketPageComponent, {
        set: {
          providers: [{ provide: AccountService, useClass: AccountServiceStub }]
        }
      })
      .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(BasketPageComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
