import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing'
import { SignUpPageComponent } from './sign-up-page.component'
import { HttpErrorResponse } from '@angular/common/http'
import { DebugElement } from '@angular/core'
import { Observable, of, throwError } from 'rxjs'
import { Router } from '@angular/router'
import { AccountService } from 'src/app/shared/services/account.service'
import { ReactiveFormsModule, FormsModule } from '@angular/forms'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { By } from '@angular/platform-browser'
import { SignUpI } from '../../shared/models/user.data'
import { MatFormFieldModule } from '@angular/material/form-field'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { MatInputModule } from '@angular/material/input'
import { MatIconModule } from '@angular/material/icon'

export class AccountServiceStub {
  signUp (signUpExample: SignUpI): Observable<{token: string}> {
    if (signUpExample.firstName === 'Broly' && signUpExample.lastName === 'DeVegeta' && signUpExample.email === 'broly@dbz.com' && signUpExample.password === 'supersayan' && signUpExample.passwordConfirmation === 'supersayan') {
      return of({ token: 'mock token' })
    }
    return throwError(new HttpErrorResponse({ status: 401 }))
  }
}

describe('SignUpPageComponent', () => {
  let component: SignUpPageComponent
  let fixture: ComponentFixture<SignUpPageComponent>
  let firstName: DebugElement
  let lastName: DebugElement
  let email: DebugElement
  let password: DebugElement
  let passwordConfirmation: DebugElement

  let validate: DebugElement
  let router: Router

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SignUpPageComponent],
      imports: [
        ReactiveFormsModule,
        FormsModule,
        HttpClientTestingModule,
        RouterTestingModule.withRoutes([]),
        BrowserAnimationsModule,
        MatFormFieldModule,
        MatInputModule,
        MatIconModule
      ]
    })
      .overrideComponent(SignUpPageComponent, {
        set: {
          providers: [{ provide: AccountService, useClass: AccountServiceStub }]
        }
      })
      .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(SignUpPageComponent)
    component = fixture.componentInstance
    fixture.detectChanges()

    firstName = fixture.debugElement.query(By.css('.firstName'))
    lastName = fixture.debugElement.query(By.css('.lastName'))
    email = fixture.debugElement.query(By.css('.email'))
    password = fixture.debugElement.query(By.css('.password'))
    passwordConfirmation = fixture.debugElement.query(By.css('.passwordConfirmation'))
    validate = fixture.debugElement.query(By.css('.submit'))
    router = getTestBed().inject(Router)
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('should have empty fields and a disable validate button at start', () => {
    expect(firstName.nativeElement.textContent).toEqual('')
    expect(lastName.nativeElement.textContent).toEqual('')
    expect(email.nativeElement.textContent).toEqual('')
    expect(password.nativeElement.textContent).toEqual('')
    expect(passwordConfirmation.nativeElement.textContent).toEqual('')
    expect(component.signUpForm.controls.firstName.errors.required).toBeTruthy()
    expect(component.signUpForm.controls.lastName.errors.required).toBeTruthy()
    expect(component.signUpForm.controls.email.errors.required).toBeTruthy()
    expect(component.signUpForm.controls.password.errors.required).toBeTruthy()
    expect(component.signUpForm.controls.passwordConfirmation.errors.required).toBeTruthy()
    expect(component.signUpForm.valid).toBeFalsy()
    expect(validate.nativeElement.disabled).toBe(true)
  })

  it('should set sign up button to enable if the form is valid', () => {
    component.signUpForm.controls.firstName.setValue('Broly')
    component.signUpForm.controls.lastName.setValue('DeVegeta')
    component.signUpForm.controls.email.setValue('broly@dbz.com')
    component.signUpForm.controls.password.setValue('supersayan')
    component.signUpForm.controls.passwordConfirmation.setValue('supersayan')
    expect(component.signUpForm.valid).toBeTruthy()
    fixture.detectChanges()
    expect(validate.nativeElement.disabled).toBe(false)
  })

  it('should set signup button to disable if the form is missing a valid email', () => {
    component.signUpForm.controls.firstName.setValue('Broly')
    component.signUpForm.controls.lastName.setValue('DeVegeta')
    component.signUpForm.controls.password.setValue('supersayan')
    component.signUpForm.controls.passwordConfirmation.setValue('supersayan')
    component.signUpForm.controls.email.setValue('Broly')
    fixture.detectChanges()
    expect(component.signUpForm.controls.email.errors.email).toBeTruthy()
    expect(component.signUpForm.valid).toBeFalsy()
    expect(validate.nativeElement.disabled).toBe(true)
  })

  it('should set signup button to disable if the password is too long', () => {
    component.signUpForm.controls.firstName.setValue('Broly')
    component.signUpForm.controls.lastName.setValue('DeVegeta')
    component.signUpForm.controls.email.setValue('broly@dbz.com')
    component.signUpForm.controls.password.setValue('1234567890123456789012345678901234567890')
    component.signUpForm.controls.passwordConfirmation.setValue('1234567890123456789012345678901234567890')
    fixture.detectChanges()
    expect(component.signUpForm.controls.password.errors.maxlength).toBeTruthy()
    expect(component.signUpForm.controls.passwordConfirmation.errors.maxlength).toBeTruthy()
    expect(component.signUpForm.valid).toBeFalsy()
    expect(validate.nativeElement.disabled).toBe(true)
  })

  it('should set signup button to disable if the password is too short', () => {
    component.signUpForm.controls.firstName.setValue('Broly')
    component.signUpForm.controls.lastName.setValue('DeVegeta')
    component.signUpForm.controls.email.setValue('broly@dbz.com')
    component.signUpForm.controls.password.setValue('test')
    component.signUpForm.controls.passwordConfirmation.setValue('test')
    fixture.detectChanges()
    expect(component.signUpForm.controls.password.errors.minlength).toBeTruthy()
    expect(component.signUpForm.controls.passwordConfirmation.errors.minlength).toBeTruthy()
    expect(component.signUpForm.valid).toBeFalsy()
    expect(validate.nativeElement.disabled).toBe(true)
  })

  it('should set signup button to disable if the form is missing identical passwords', () => {
    component.signUpForm.controls.firstName.setValue('Broly')
    component.signUpForm.controls.lastName.setValue('DeVegeta')
    component.signUpForm.controls.email.setValue('broly@dbz.com')
    component.signUpForm.controls.password.setValue('supersayan')
    component.signUpForm.controls.passwordConfirmation.setValue('supersayan2')
    expect(component.signUpForm.valid).toBeFalsy()
    expect(validate.nativeElement.disabled).toBe(true)
  })

  it('should set signup button to disable if the form is missing a valid first name', () => {
    component.signUpForm.controls.lastName.setValue('DeVegeta')
    component.signUpForm.controls.email.setValue('broly@dbz.com')
    component.signUpForm.controls.password.setValue('supersayan')
    component.signUpForm.controls.passwordConfirmation.setValue('supersayan')

    component.signUpForm.controls.firstName.setValue('IAmAThirtyOneLettersFirstName31')
    expect(component.signUpForm.controls.firstName.errors.maxlength).toBeTruthy()
    expect(component.signUpForm.valid).toBeFalsy()
    expect(validate.nativeElement.disabled).toBe(true)
  })

  it('should set signup button to disable if the form is missing a valid last name', () => {
    component.signUpForm.controls.firstName.setValue('Broly')
    component.signUpForm.controls.email.setValue('broly@dbz.com')
    component.signUpForm.controls.password.setValue('supersayan')
    component.signUpForm.controls.passwordConfirmation.setValue('supersayan')
    component.signUpForm.controls.lastName.setValue('IAmAThirtyOneLettersLastName_31')
    expect(component.signUpForm.valid).toBeFalsy()
    expect(validate.nativeElement.disabled).toBe(true)
  })

  it('should call the onValidate method', () => {
    const onValidate = spyOn(component, 'onSubmit')
    component.signUpForm.controls.firstName.setValue('Broly')
    component.signUpForm.controls.lastName.setValue('DeVegeta')
    component.signUpForm.controls.email.setValue('broly@dbz.com')
    component.signUpForm.controls.password.setValue('supersayan')
    component.signUpForm.controls.passwordConfirmation.setValue('supersayan')
    fixture.detectChanges()
    validate.nativeElement.click()
    expect(onValidate).toHaveBeenCalled()
  })

  it('should navigate to home page if the sign up succeed', () => {
    const routerSpy = spyOn(router, 'navigate')
    component.signUpForm.controls.firstName.setValue('Broly')
    component.signUpForm.controls.lastName.setValue('DeVegeta')
    component.signUpForm.controls.email.setValue('broly@dbz.com')
    component.signUpForm.controls.password.setValue('supersayan')
    component.signUpForm.controls.passwordConfirmation.setValue('supersayan')
    fixture.detectChanges()
    validate.nativeElement.click()
    expect(routerSpy).toHaveBeenCalledWith(['/'])
  })
})
