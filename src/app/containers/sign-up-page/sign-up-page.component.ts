import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { AccountService } from 'src/app/shared/services/account.service'
import { Router } from '@angular/router'
// import custom validator to validate that password and confirm password fields match
import { MustMatch } from 'src/app/shared/validators/must-match.validator'
import { SignUpI } from '../../shared/models/user.data'

@Component({
  selector: 'app-sign-up-page',
  templateUrl: './sign-up-page.component.html',
  styleUrls: ['./sign-up-page.component.scss']
})
export class SignUpPageComponent implements OnInit {
  signUpForm: FormGroup
  showError: boolean
  hidePassword: boolean
  hidePasswordConfirmation: boolean

  constructor (private formBuilder: FormBuilder,
              private accountService: AccountService,
              private router: Router) {
    this.signUpForm = this.formBuilder.group({
      firstName: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(30)]],
      lastName: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(30)]],
      email: ['', [Validators.email, Validators.required]],
      password: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(30)]],
      passwordConfirmation: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(30)]]
    }, {
      validator: MustMatch('password', 'passwordConfirmation')
    })
  }

  ngOnInit (): void {
    this.showError = false
    this.hidePassword = true
    this.hidePasswordConfirmation = true
  }

  displayNameError (): string {
    if (this.signUpForm.controls.firstName.hasError('required') || this.signUpForm.controls.lastName.hasError('required')) {
      return 'Vous devez entrer une valeur'
    }
  }

  displayEmailError (): string {
    if (this.signUpForm.controls.email.hasError('required')) {
      return 'Vous devez entrer une valeur'
    }

    if (this.signUpForm.controls.email.hasError('email')) {
      return 'Votre email n\'est pas valide'
    }
  }

  displayPasswordError (): string {
    if (this.signUpForm.controls.password.hasError('required')) {
      return 'Vous devez entrer une valeur'
    }

    if (this.signUpForm.controls.password.hasError('minlength')) {
      return 'Votre mot de passe est trop court'
    }

    if (this.signUpForm.controls.password.hasError('maxlength')) {
      return 'Votre mot de passe est trop long'
    }
  }

  displayPasswordConfirmationError (): string {
    if (this.signUpForm.controls.passwordConfirmation.hasError('required')) {
      return 'Vous devez entrer une valeur'
    }

    if (this.signUpForm.controls.passwordConfirmation.hasError('minlength')) {
      return 'Votre mot de passe est trop court'
    }

    if (this.signUpForm.controls.passwordConfirmation.hasError('maxlength')) {
      return 'Votre mot de passe est trop long'
    }
  }

  onSubmit ({ value }: { value: SignUpI }): void {
    // stop here if form is invalid
    if (this.signUpForm.invalid) {
      return
    }

    this.accountService.signUp(value).subscribe(
      () => { this.router.navigate(['/']) },
      error => {
        console.log(error) // TODO need to handle error
        this.showError = true
      }
    )
  }
}
