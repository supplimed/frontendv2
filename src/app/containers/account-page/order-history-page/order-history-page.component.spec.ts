import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { OrderHistoryPageComponent } from './order-history-page.component'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { RouterTestingModule } from '@angular/router/testing'

describe('OrderHistoryPageComponent', () => {
  let component: OrderHistoryPageComponent
  let fixture: ComponentFixture<OrderHistoryPageComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrderHistoryPageComponent],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule
      ]
    })
      .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderHistoryPageComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
