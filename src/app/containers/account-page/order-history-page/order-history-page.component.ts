import { Component, OnInit } from '@angular/core'
import { BasketService } from '../../../shared/services/basket.service'
import { OrderHistoryI } from '../../../shared/models/orderHistory.data'

@Component({
  selector: 'app-order-history-page',
  templateUrl: './order-history-page.component.html',
  styleUrls: ['./order-history-page.component.scss']
})
export class OrderHistoryPageComponent implements OnInit {
  orderHistories: OrderHistoryI[]

  constructor (private basketService: BasketService) {
  }

  ngOnInit (): void {
    this.basketService.getOrderHistory().subscribe(data => {
      this.orderHistories = data
    })
  }
}
