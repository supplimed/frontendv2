import { Component } from '@angular/core'
import { AccountService } from '../../shared/services/account.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-account-page',
  templateUrl: './account-page.component.html',
  styleUrls: ['./account-page.component.scss']
})
export class AccountPageComponent {
  constructor (private accountService: AccountService,
              private router: Router) { }

  disconnect (): void {
    this.accountService.logOut()
    this.router.navigate(['/'])
  }
}
