import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { AccountPageComponent } from './account-page.component'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { MatIconTestingModule } from '@angular/material/icon/testing'
import { MatIcon } from '@angular/material/icon'

describe('AccountPageComponent', () => {
  let component: AccountPageComponent
  let fixture: ComponentFixture<AccountPageComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AccountPageComponent, MatIcon],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        MatIconTestingModule
      ]
    })
      .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountPageComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
