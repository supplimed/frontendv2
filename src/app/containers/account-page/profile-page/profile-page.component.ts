import { Component } from '@angular/core'
import { AccountService } from '../../../shared/services/account.service'
import { Router } from '@angular/router'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.scss']
})
export class ProfilePageComponent {
  profileForm: FormGroup

  constructor (private formBuilder: FormBuilder,
               private router: Router,
               public accountService: AccountService) {
    this.profileForm = this.formBuilder.group({
      firstName: [accountService.getCurrentUser().firstName, [Validators.required]],
      lastName: [accountService.getCurrentUser().lastName, [Validators.required]],
      email: [accountService.getCurrentUser().email, [Validators.email, Validators.required]]
    })
  }

  displayNameError (): string {
    if (this.profileForm.controls.firstName.hasError('required') || this.profileForm.controls.lastName.hasError('required')) {
      return 'Vous devez entrer une valeur'
    }
  }

  displayEmailError (): string {
    if (this.profileForm.controls.email.hasError('required')) {
      return 'Vous devez entrer une valeur'
    }

    if (this.profileForm.controls.email.hasError('email')) {
      return 'Votre email n\'est pas valide'
    }
  }

  disconnect (): void {
    this.accountService.logOut()
    this.router.navigate(['/'])
  }
}
