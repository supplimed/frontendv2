import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { ProfilePageComponent } from './profile-page.component'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { MatIconModule, MatIconRegistry } from '@angular/material/icon'
import { AccountService } from '../../../shared/services/account.service'
import { FakeMatIconRegistry } from '@angular/material/icon/testing'
import { UserData } from '../../../shared/models/user.data'

export class AccountServiceStub {
  getCurrentUser (): UserData {
    return {
      firstName: 'gogeta',
      lastName: 'deNamek',
      email:'gogeta.denamek@dbz.com',
      token:'$2b$10$cx3cXA7Tg/phv3lCvrJ13.QBb3z9Z3EBkcxIFDazMq/67aaeq6Qwy',
      id:'42',
      mpClientId: null,
      mpWalletId: null,
      mpCardId: null,
      addresses: null
    }
  }
}

describe('ProfilePageComponent', () => {
  let component: ProfilePageComponent
  let fixture: ComponentFixture<ProfilePageComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProfilePageComponent],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        MatIconModule
      ],
      providers: [
        AccountService,
        { provide: MatIconRegistry, useClass: FakeMatIconRegistry }
      ]
    })
      .overrideComponent(ProfilePageComponent, {
        set: {
          providers: [{ provide: AccountService, useClass: AccountServiceStub }]
        }
      })
      .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfilePageComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
