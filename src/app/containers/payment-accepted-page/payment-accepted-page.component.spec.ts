import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { PaymentAcceptedPageComponent } from './payment-accepted-page.component'
import { RouterTestingModule } from '@angular/router/testing'

describe('PaymentAcceptedPageComponent', () => {
  let component: PaymentAcceptedPageComponent
  let fixture: ComponentFixture<PaymentAcceptedPageComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PaymentAcceptedPageComponent],
      imports: [RouterTestingModule]
    })
      .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentAcceptedPageComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
