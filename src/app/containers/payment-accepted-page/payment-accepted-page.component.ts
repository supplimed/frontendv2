import { Component } from '@angular/core'
import { Router } from '@angular/router'

@Component({
  selector: 'app-payment-accepted-page',
  templateUrl: './payment-accepted-page.component.html',
  styleUrls: ['./payment-accepted-page.component.scss']
})
export class PaymentAcceptedPageComponent {
  constructor (private router: Router) {
  }

  goToHome (): void {
    this.router.navigate(['/'])
  }
}
