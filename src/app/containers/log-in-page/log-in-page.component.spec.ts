import { async, ComponentFixture, getTestBed, TestBed } from '@angular/core/testing'
import { LogInPageComponent } from './log-in-page.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { RouterTestingModule } from '@angular/router/testing'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { By } from '@angular/platform-browser'
import { AccountService } from '../../shared/services/account.service'
import { Observable, of, throwError } from 'rxjs'
import { Router } from '@angular/router'
import { HttpErrorResponse } from '@angular/common/http'
import { DebugElement } from '@angular/core'
import { LogInI } from '../../shared/models/user.data'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { MatFormFieldModule } from '@angular/material/form-field'
import { MatInputModule } from '@angular/material/input'
import { MatIconModule } from '@angular/material/icon'

export class AccountServiceStub {
  logIn (logInExample: LogInI): Observable<{token: string} > {
    if (logInExample.email === 'gogeta@dbz.com' && logInExample.password === 'supersayan') {
      return of({ token: 'mock token' })
    }
    return throwError(new HttpErrorResponse({ status: 401 }))
  }
}

describe('LogInPageComponent', () => {
  let component: LogInPageComponent
  let fixture: ComponentFixture<LogInPageComponent>
  let email: DebugElement
  let password: DebugElement
  let validate: DebugElement
  let router: Router

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LogInPageComponent],
      imports: [
        ReactiveFormsModule,
        FormsModule,
        HttpClientTestingModule,
        RouterTestingModule.withRoutes([]),
        BrowserAnimationsModule,
        MatFormFieldModule,
        MatInputModule,
        MatIconModule
      ]
    })
      .overrideComponent(LogInPageComponent, {
        set: {
          providers: [{ provide: AccountService, useClass: AccountServiceStub }]
        }
      })
      .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(LogInPageComponent)
    component = fixture.componentInstance
    fixture.detectChanges()

    email = fixture.debugElement.query(By.css('.email'))
    password = fixture.debugElement.query(By.css('.password'))
    validate = fixture.debugElement.query(By.css('.submit'))
    router = getTestBed().inject(Router)
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('should have empty email and password field and a disable validate button at start', () => {
    expect(email.nativeElement.textContent).toEqual('')
    expect(password.nativeElement.textContent).toEqual('')
    expect(component.logInForm.controls.email.errors.required).toBeTruthy()
    expect(component.logInForm.controls.password.errors.required).toBeTruthy()
    expect(component.logInForm.valid).toBeFalsy()
    expect(validate.nativeElement.disabled).toBe(true)
  })

  it('should set login button to enable if the form is valid', () => {
    component.logInForm.controls.email.setValue('gogeta@dbz.com')
    component.logInForm.controls.password.setValue('supersayan')
    expect(component.logInForm.valid).toBeTruthy()
    fixture.detectChanges()
    expect(validate.nativeElement.disabled).toBe(false)
  })

  it('should set login button to disable if the email field is empty', () => {
    component.logInForm.controls.email.setValue('')
    component.logInForm.controls.password.setValue('supersayan')
    fixture.detectChanges()
    expect(component.logInForm.controls.email.errors.required).toBeTruthy()
    expect(component.logInForm.valid).toBeFalsy()
    expect(validate.nativeElement.disabled).toBe(true)
  })

  it('should set login button to disable if the email field is not filled with an email', () => {
    component.logInForm.controls.email.setValue('gogeta')
    fixture.detectChanges()
    expect(component.logInForm.controls.email.errors.email).toBeTruthy()
    expect(component.logInForm.valid).toBeFalsy()
    expect(validate.nativeElement.disabled).toBe(true)
  })

  it('should set login button to disable if the password is too short', () => {
    component.logInForm.controls.email.setValue('gogeta@dbz.com')
    component.logInForm.controls.password.setValue('super')
    fixture.detectChanges()
    expect(component.logInForm.controls.password.errors.minlength).toBeTruthy()
    expect(component.logInForm.valid).toBeFalsy()
    expect(validate.nativeElement.disabled).toBe(true)
  })

  it('should set login button to disable if the password is too long', () => {
    component.logInForm.controls.email.setValue('gogeta@dbz.com')
    component.logInForm.controls.password.setValue('1234567890123456789012345678901234567890')
    fixture.detectChanges()
    expect(component.logInForm.controls.password.errors.maxlength).toBeTruthy()
    expect(component.logInForm.valid).toBeFalsy()
    expect(validate.nativeElement.disabled).toBe(true)
  })

  it('should call the onValidate method', () => {
    const onValidate = spyOn(component, 'onSubmit')
    component.logInForm.controls.email.setValue('gogeta@dbz.com')
    component.logInForm.controls.password.setValue('supersayan')
    fixture.detectChanges()
    validate.nativeElement.click()
    expect(onValidate).toHaveBeenCalled()
  })

  it('should navigate to home page if the login succeed', () => {
    const routerSpy = spyOn(router, 'navigate')
    component.logInForm.controls.email.setValue('gogeta@dbz.com')
    component.logInForm.controls.password.setValue('supersayan')
    fixture.detectChanges()
    validate.nativeElement.click()
    expect(routerSpy).toHaveBeenCalledWith(['/'])
  })
})
