import { Component } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { AccountService } from '../../shared/services/account.service'
import { Router } from '@angular/router'
import { LogInI } from '../../shared/models/user.data'
import { BasketService } from '../../shared/services/basket.service'

@Component({
  selector: 'app-log-in-page',
  templateUrl: './log-in-page.component.html',
  styleUrls: ['./log-in-page.component.scss']
})
export class LogInPageComponent {
  logInForm: FormGroup
  hide: boolean

  constructor (private formBuilder: FormBuilder,
               private accountService: AccountService,
               private basketService: BasketService,
               private router: Router) {
    this.logInForm = this.formBuilder.group({
      email: ['', [Validators.email, Validators.required]],
      password: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(30)]]
    })
  }

  displayEmailError (): string {
    if (this.logInForm.controls.email.hasError('required')) {
      return 'Vous devez entrer une valeur'
    }

    if (this.logInForm.controls.email.hasError('email')) {
      return 'Votre email n\'est pas valide'
    }
  }

  displayPasswordError (): string {
    if (this.logInForm.controls.password.hasError('required')) {
      return 'Vous devez entrer une valeur'
    }

    if (this.logInForm.controls.password.hasError('minlength')) {
      return 'Votre mot de passe est trop court'
    }

    if (this.logInForm.controls.password.hasError('maxlength')) {
      return 'Votre mot de passe est trop long'
    }
  }

  onSubmit ({ value }: { value: LogInI }): void {
    this.accountService.logIn(value).subscribe(
      () => {
        this.basketService.getBasket().subscribe()
        this.router.navigate(['/'])
      },
      error => {
        console.log(error) // TODO need to handle error
      }
    )
  }
}
