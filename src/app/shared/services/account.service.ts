import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { BehaviorSubject, Observable } from 'rxjs'
import { map } from 'rxjs/operators'
import { LogInI, SignUpI, UserData } from '../models/user.data'
import { serverUrl } from '../config'

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  private currentUserSubject: BehaviorSubject<UserData>
  public currentUser$: Observable<UserData>

  constructor (private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<UserData>(JSON.parse(localStorage.getItem('currentUser')))
    this.currentUser$ = this.currentUserSubject.asObservable()
  }

  public logIn (logIn: LogInI): Observable<UserData> {
    return this.http.post(serverUrl + 'authentication/login', logIn)
      .pipe(map((data: {user: UserData, token: string}) => {
        const currentUser = new UserData(data.user, data.token)
        localStorage.setItem('currentUser', JSON.stringify(currentUser))
        this.currentUserSubject.next(currentUser)
        return currentUser
      }))
  }

  public signUp (signUp: SignUpI): Observable<UserData> {
    return this.http.post(serverUrl + 'authentication/signup', signUp)
      .pipe(map((data: {user: UserData, token: string}) => {
        const currentUser = new UserData(data.user, data.token)
        localStorage.setItem('currentUser', JSON.stringify(currentUser))
        this.currentUserSubject.next(currentUser)
        return currentUser
      }))
  }

  public logOut (): void{
    localStorage.removeItem('currentUser')
    this.currentUserSubject.next(null)
  }

  public getCurrentUser (): UserData {
    return this.currentUserSubject.value
  }
}
