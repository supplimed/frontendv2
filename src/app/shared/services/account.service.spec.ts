/* tslint:disable:no-unused-variable */
import { async, TestBed } from '@angular/core/testing'
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'
import { AccountService } from './account.service'
import { LogInI, UserData } from '../models/user.data'
import { serverUrl } from '../config'

describe('AccountService', () => {
  let service: AccountService
  let httpTestingController: HttpTestingController

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    })
  }))

  beforeEach(() => {
    httpTestingController = TestBed.inject(HttpTestingController)
    service = TestBed.inject(AccountService)
  })

  afterEach(() => {
    httpTestingController.verify()
  })

  it('should create', () => {
    expect(service).toBeTruthy()
  })

  it('should log out the user', () => {
    localStorage.setItem('currentUser', 'blabla')
    expect(localStorage.getItem('currentUser')).toEqual('blabla')
    service.logOut()
    expect(localStorage.getItem('currentUser')).toBeNull()
  })

  it('should get the token if the email and password match a user', () => {
    const mockResponse = {
      user: {
        id: '42',
        email: 'gogeta@dbz.com',
        firstName: 'gogeta',
        lastName: 'namek'
      },
      token: '$2b$10$cx3cXA7Tg/phv3lCvrJ13.QBb3z9Z3EBkcxIFDazMq/67aaeq6Qwy'
    }
    const targetCurrentUser = new UserData({ email:'gogeta@dbz.com', id:'42', lastName:'namek', firstName:'gogeta', token:'', mpClientId: undefined, mpWalletId: undefined, mpCardId: undefined, addresses: undefined }, '$2b$10$cx3cXA7Tg/phv3lCvrJ13.QBb3z9Z3EBkcxIFDazMq/67aaeq6Qwy')
    const logInExample: LogInI = {
      email: 'validEmail',
      password: 'validPassword'
    }

    service.logIn(logInExample).subscribe(test => {
      expect(test).toEqual(targetCurrentUser)
    })

    const req = httpTestingController.expectOne(serverUrl + 'authentication/login')
    expect(req.request.method).toEqual('POST')
    req.flush(mockResponse)

    expect(localStorage.getItem('currentUser')).toEqual(JSON.stringify(targetCurrentUser))
  })
})
