import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { serverUrl } from '../config'
import { AddressI } from '../models/address.data'
import { UserData } from '../models/user.data'
import { map } from 'rxjs/operators'
import { Observable } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor (private http: HttpClient) {
  }

  public addUserAddress (address: AddressI): Observable<UserData> {
    return this.http.post<UserData>(serverUrl + 'api/v1/users/address/add', { address })
  }

  public getUser (): Observable<UserData> {
    return this.http.get<{data: UserData}>(serverUrl + 'api/v1/users/me')
      .pipe(map((res: { data: UserData}) => {
        return res.data
      }))
  }
}
