import { Injectable } from '@angular/core'
import { Observable, Subject } from 'rxjs'
import { serverUrl } from '../config'
import { HttpClient } from '@angular/common/http'
import { ProductToSendI } from '../models/product.data'
import { map } from 'rxjs/operators'
import { BasketI } from '../models/basket.data'
import { AddressI } from '../models/address.data'
import { OrderI } from '../models/order.data'
import { OrderHistoryI } from '../models/orderHistory.data'

@Injectable({
  providedIn: 'root'
})
export class BasketService {
  private basketSubject: Subject<BasketI>
  public basket$: Observable<BasketI>

  constructor (private http: HttpClient) {
    this.basketSubject = new Subject<BasketI>()
    this.basket$ = this.basketSubject.asObservable()
  }

  public getBasket (): Observable<BasketI> {
    return this.http.get<BasketI>(serverUrl + 'api/v1/basket/me')
      .pipe(map((data) => {
        this.basketSubject.next(data)
        return data
      }))
  }

  public addBasket (products: ProductToSendI[]): Observable<BasketI> {
    return this.http.post<BasketI>(serverUrl + 'api/v1/basket/products/add', { products })
      .pipe(map((data) => {
        this.basketSubject.next(data)
        return data
      }))
  }

  public addOrderAddress (deliveryAddress: AddressI, billingAddress: AddressI): Observable<OrderI> {
    return this.http.put<OrderI>(serverUrl + 'api/v1/order/address', { deliveryAddress, billingAddress })
  }

  public getOrder (): Observable<OrderI> {
    return this.http.get<{order: OrderI}>(serverUrl + 'api/v1/order')
      .pipe(map((data) => {
        return data.order
      }))
  }

  public setBasketQuantity (products: ProductToSendI[]): Observable<BasketI> {
    return this.http.put<BasketI>(serverUrl + 'api/v1/basket/products/set', { products })
      .pipe(map((data) => {
        this.basketSubject.next(data)
        return data
      }))
  }

  public getOrderHistory (): Observable<OrderHistoryI[]> {
    return this.http.get<{orderHistory: OrderHistoryI[]}>(serverUrl + 'api/v1/order/history')
      .pipe(map((res: {orderHistory: OrderHistoryI[]}) => {
        return res.orderHistory
      }))
  }

  public getOrderHistoryById (id: string): Observable<OrderHistoryI> {
    return this.http.get<{orderHistory: OrderHistoryI}>(serverUrl + 'api/v1/order/history/' + id)
      .pipe(map((res: {orderHistory: OrderHistoryI}) => {
        return res.orderHistory
      }))
  }
}
