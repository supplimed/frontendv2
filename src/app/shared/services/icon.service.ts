import { Injectable } from '@angular/core'
import { MatIconRegistry } from '@angular/material/icon'
import { DomSanitizer } from '@angular/platform-browser'

@Injectable({
  providedIn: 'root'
})
export class IconService {
  constructor (private matIconRegistry: MatIconRegistry,
              private domSanitizer: DomSanitizer) {
    this.matIconRegistry.addSvgIcon('logo', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/supplimedLogo.svg'))
    this.matIconRegistry.addSvgIcon('basket', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/shoppingBasket.svg'))
    this.matIconRegistry.addSvgIcon('profile', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/profile.svg'))
    this.matIconRegistry.addSvgIcon('add', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/add.svg'))
    this.matIconRegistry.addSvgIcon('remove', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/remove.svg'))
    this.matIconRegistry.addSvgIcon('arrowLeft', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/arrowLeft.svg'))
    this.matIconRegistry.addSvgIcon('arrowRight', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/arrowRight.svg'))
    this.matIconRegistry.addSvgIcon('account', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/accountCircle.svg'))
    this.matIconRegistry.addSvgIcon('disconnect', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/disconnect.svg'))
    this.matIconRegistry.addSvgIcon('clear', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/clear.svg'))
    this.matIconRegistry.addSvgIcon('visa', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/visa.svg'))
    this.matIconRegistry.addSvgIcon('creditCard', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/creditCard.svg'))
  }
}
