import { Injectable } from '@angular/core'
import { HttpClient, HttpParams } from '@angular/common/http'
import { Observable, Subject } from 'rxjs'
import { serverUrl } from '../config'
import { map } from 'rxjs/operators'
import { PaginationI, ProductData } from '../models/product.data'

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private productsSubject: Subject<{ data: ProductData[], pagination: PaginationI }>
  public products$: Observable<{ data: ProductData[], pagination: PaginationI }>

  constructor (private http: HttpClient) {
    this.productsSubject = new Subject<{ data: ProductData[], pagination: PaginationI }>()
    this.products$ = this.productsSubject.asObservable()
  }

  public getProducts (page = 1, pageSize = 20): Observable<{ data: ProductData[], pagination: PaginationI }> {
    let params: HttpParams = new HttpParams()
    params = params.append('page', page.toString())
    params = params.append('pageSize', pageSize.toString())

    return this.http.get<{data: ProductData[], pagination: PaginationI}>(serverUrl + 'products', { params: params })
      .pipe(map((productData) => {
        this.productsSubject.next(productData)
        return productData
      }))
  }

  public getProductById (id: number): Observable<{data: ProductData}> {
    return this.http.get<{data: ProductData}>(serverUrl + 'products/' + id)
  }
}
