import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { mangoPayBaseURL, mangoPayClientId, serverUrl } from '../config'
import { HttpClient } from '@angular/common/http'
import * as mangoPay from 'mangopay-cardregistration-js-kit'
import {
  CardRegistrationDataI, FullCardRegistrationDataI,
  MangoPayCardI, MangoPayFullLegalUserI, MangoPayFullWalletI, MangoPayRegisteredCardI,
  MangoPayRegistrationCardI
} from '../models/mangoPay.data'

class MangoPayLegalUserI {
}

@Injectable({
  providedIn: 'root'
})
export class PaymentService {
  constructor (private http: HttpClient) { }

  public addCostumer (user: MangoPayLegalUserI): Observable<{ user: MangoPayFullLegalUserI, wallet: MangoPayFullWalletI }> {
    return this.http.post<{ user: MangoPayFullLegalUserI, wallet: MangoPayFullWalletI }>(serverUrl + 'api/v1/mangopay/createLegalUser', { user })
  }

  public createCardRegistration (): Observable<{cardRegistrationData: CardRegistrationDataI, status: number}> {
    return this.http.get<{cardRegistrationData: CardRegistrationDataI, status: number}>(serverUrl + 'api/v1/mangopay/cardregistration/create')
  }

  public saveCardRegistration (fullCardRegistrationData: FullCardRegistrationDataI): Observable<{status: number}> {
    return this.http.post<{status: number}>(serverUrl + 'api/v1/mangopay/cardregistration/save', fullCardRegistrationData)
  }

  public directCardPayIn (cardId: string): Observable<{ mpStatus: string, resultCode: string}> {
    return this.http.post<{ mpStatus: string, resultCode: string}>(serverUrl + 'api/v1/mangopay/payin/card/direct', { cardId })
  }

  public getUserCards (): Observable<{ cards: MangoPayRegisteredCardI[] }> {
    return this.http.get<{ cards: MangoPayRegisteredCardI[] }>(serverUrl + 'api/v1/mangopay/user/cards')
  }

  public sendCardDetailForTokenization (mangoPayCardRegistration: MangoPayRegistrationCardI, cardData: MangoPayCardI): void {
    mangoPay.cardRegistration.baseURL = mangoPayBaseURL
    mangoPay.cardRegistration.clientId = mangoPayClientId
    mangoPay.cardRegistration.init(mangoPayCardRegistration)

    mangoPay.cardRegistration.registerCard(
      cardData,
      (res) => {
        this.saveCardRegistration(res).subscribe()
      },
      (err) => {
        throw err // TODO manage error
      }
    )
  }
}
