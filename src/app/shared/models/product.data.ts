export const enum CURRENCIES {
  EURO = 'euro',
}

export interface PaginationI {
  page: number
  pageSize: number
  pageCount: number
  totalCount: number
}

export interface ProductToSendI {
  id: string
  quantity: number
}

export class ProductData {
  public id: string
  public name: string
  public price: number
  public currency: CURRENCIES
  public description: string
  public imageUrl: string
  public type: string

  constructor (product: ProductData) {
    this.id = product.id
    this.name = product.name
    this.price = product.price
    this.currency = product.currency
    this.description = product.description
    this.imageUrl = product.imageUrl
    this.type = product.type
  }
}
