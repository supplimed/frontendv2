import { ProductData } from './product.data'

export interface BasketElementI {
  product: ProductData
  quantity: number
  bulkPrice: number
}

export interface BasketI {
  basketElements: BasketElementI[]
  totalPrice: number
  totalProductsQuantity: number
}
