import { AddressI } from './address.data'
import { BasketI } from './basket.data'

export interface OrderI {
  deliveryAddress: AddressI
  billingAddress: AddressI
  basket: BasketI
  id: string
}
