export interface MangoPayRegistrationCardI {
  accessKey: string
  preregistrationData: string
  cardRegistrationURL: string
  Id: string
}

export interface MangoPayCardI {
  cardNumber: string
  cardExpirationDate: string
  cardCvx: string
  cardType: string
}

export interface MangoPayCardFormI {
  cardNumber: string
  cardExpirationMonthDate: string
  cardExpirationYearDate: string
  cardCvx: string
  cardType: string
}

export interface MangoPayLegalUserI {
  LegalPersonType: string
  Name: string
  LegalRepresentativeBirthday: number
  LegalRepresentativeCountryOfResidence: string
  LegalRepresentativeNationality: string
  LegalRepresentativeFirstName: string
  LegalRepresentativeLastName: string
  Email: string
}

export interface MangoPayRegisteredCardI {
  expirationDate: string
  alias: string
  cardType: string
  id: string
}

export interface MangoPayFullLegalUserI {
  CompanyNumber: string
  Email: string
  HeadQuartersAddress: string
  Id: string
  KYCLevel: string
  LegalPersonType: string
  LegalRepresentativeAddress: string
  LegalRepresentativeBirthday: number
  LegalRepresentativeCountryOfResidence: string
  LegalRepresentativeEmail: string
  LegalRepresentativeFirstName: string
  LegalRepresentativeLastName: string
  LegalRepresentativeNationality: string
  LegalRepresentativeProofOfIdentity: string
  Name: string
  PersonType: string
  ProofOfRegistration: string
  ShareholderDeclaration: string
  Statute: string
  Tag: string
}

export interface MangoPayFullWalletI {
  Balance: MagoPayMoneyI
  Currency: string
  Description: string
  FundsType: string
  Id: string
  Owners: string[]
  Tag: string
}

export interface CardRegistrationDataI {
  AccessKey: string
  CardRegistrationURL: string
  Id: string
  PreregistrationData: string
}

export interface FullCardRegistrationDataI {
  AccessKey: string
  CardId: string
  CardRegistrationURL: string
  CardType: string
  CreationDate: number
  Currency: string
  Id: string
  PreregistrationData: string
  RegistrationData: string
  ResultCode: string
  ResultMessage: string
  Status: string
  Tag: string
  UserId: string
}

export interface MagoPayMoneyI {
  Currency: string
  Amount: number
}
