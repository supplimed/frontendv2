import { AddressI } from './address.data'
import { CURRENCIES } from './product.data'

export interface OrderHistoryI {
  id: string
  date: number
  mpTransactionId: string
  userId: string
  order: {
    deliveryAddress: AddressI
    billingAddress: AddressI
    basket: {
      totalPrice: number
      totalProductsQuantity: number
      basketElements: [
        {
          quantity: number
          bulkPrice: number
          product: {
            id: string
            name: string
            price: number
            currency: CURRENCIES
            description: string
            imageUrl: string
          }
        }
      ]
    }
  }
}
