export interface AddressI {
  address: string
  city: string
  postalCode: string
}
