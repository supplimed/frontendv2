import { AddressI } from './address.data'

export class UserData {
  public id: string
  public email: string
  public firstName: string
  public lastName: string
  public token: string
  public mpClientId: string
  public mpWalletId: string
  public mpCardId: string
  public addresses: AddressI[]

  constructor (user: UserData, token: string) {
    this.id = user.id
    this.email = user.email
    this.firstName = user.firstName
    this.lastName = user.lastName
    this.token = token
    this.mpClientId = user.mpClientId
    this.mpWalletId = user.mpWalletId
    this.mpCardId = user.mpCardId
    this.addresses = user.addresses
  }
}

export interface SignUpI {
  firstName: string
  lastName: string
  email: string
  password: string
  passwordConfirmation: string
}

export interface LogInI {
  email: string
  password: string
}
