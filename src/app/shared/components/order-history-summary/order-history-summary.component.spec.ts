import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { OrderHistorySummaryComponent } from './order-history-summary.component'

describe('OrderHistorySummaryComponent', () => {
  let component: OrderHistorySummaryComponent
  let fixture: ComponentFixture<OrderHistorySummaryComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrderHistorySummaryComponent]
    })
      .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderHistorySummaryComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
