import { Component, Input } from '@angular/core'

@Component({
  selector: 'app-order-history-summary',
  templateUrl: './order-history-summary.component.html',
  styleUrls: ['./order-history-summary.component.scss']
})
export class OrderHistorySummaryComponent {
  @Input() orderId: string
  @Input() totalPrice: number
  @Input() date: number
}
