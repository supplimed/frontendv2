import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { BasketElementComponent } from './basket-element.component'
import { HttpClientTestingModule } from '@angular/common/http/testing'

describe('BasketElementComponent', () => {
  let component: BasketElementComponent
  let fixture: ComponentFixture<BasketElementComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      declarations: [BasketElementComponent]
    })
      .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(BasketElementComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
