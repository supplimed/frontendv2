import { Component, Input, OnInit } from '@angular/core'
import { ProductData } from '../../models/product.data'
import { BasketService } from '../../services/basket.service'

@Component({
  selector: 'app-basket-element',
  templateUrl: './basket-element.component.html',
  styleUrls: ['./basket-element.component.scss']
})
export class BasketElementComponent implements OnInit {
  @Input() product: ProductData
  @Input() quantity: number
  @Input() bulkPrice: number
  @Input() interactive: boolean

  constructor (private basketService: BasketService) {
  }

  ngOnInit (): void {
    if (this.interactive === undefined) {
      this.interactive = true
    }
  }

  public removeProduct ($event): void{
    $event.stopPropagation()
    if (this.quantity >= 0) {
      const products = [{
        id: this.product.id,
        quantity: this.quantity - 1
      }]
      this.basketService.setBasketQuantity(products).subscribe()
    }
  }

  public addProduct ($event): void{
    $event.stopPropagation()
    const products = [{
      id: this.product.id,
      quantity: this.quantity + 1
    }]
    this.basketService.setBasketQuantity(products).subscribe()
  }

  public clearProduct ($event): void{
    $event.stopPropagation()
    const products = [{
      id: this.product.id,
      quantity: 0
    }]
    this.basketService.setBasketQuantity(products).subscribe()
  }
}
