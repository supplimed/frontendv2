import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { RegisteredCardComponent } from './registered-card.component'
import { CardAlliasPipe } from '../../core/cardAllias.pipe'
import { CardDatePipe } from '../../core/cardDate.pipe'
import { MatIconModule } from '@angular/material/icon'

describe('RegisteredCardComponent', () => {
  let component: RegisteredCardComponent
  let fixture: ComponentFixture<RegisteredCardComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        RegisteredCardComponent,
        CardAlliasPipe,
        CardDatePipe
      ],
      imports: [
        MatIconModule
      ]
    })
      .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisteredCardComponent)
    component = fixture.componentInstance
    component.card = {
      alias: '',
      id: '',
      expirationDate: '',
      cardType: ''
    }
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('should set cardTypeImg to visa', () => {
    component.setCardTypeImg('CB_VISA_MASTERCARD')
    expect(component.cardTypeImg).toEqual('visa')
  })
})
