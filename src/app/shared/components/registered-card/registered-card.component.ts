import { Component, Input, OnInit } from '@angular/core'
import { MangoPayRegisteredCardI } from '../../models/mangoPay.data'

@Component({
  selector: 'app-registered-card',
  templateUrl: './registered-card.component.html',
  styleUrls: ['./registered-card.component.scss']
})
export class RegisteredCardComponent implements OnInit {
  @Input() card: MangoPayRegisteredCardI
  cardTypeImg: string

  ngOnInit (): void {
    this.setCardTypeImg(this.card.cardType)
  }

  setCardTypeImg (cardType: string): void {
    if (cardType === 'CB_VISA_MASTERCARD') {
      this.cardTypeImg = 'visa'
    }
  }
}
