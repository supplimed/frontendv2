import { Component } from '@angular/core'
import { AccountService } from '../../services/account.service'
import { Router } from '@angular/router'
import { BasketService } from '../../services/basket.service'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  constructor (public accountService: AccountService,
               public basketService: BasketService,
               public router: Router) {
  }

  disconnect (): void {
    this.accountService.logOut()
    this.router.navigate(['/'])
  }
}
