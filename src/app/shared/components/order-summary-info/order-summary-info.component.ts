import { Component, EventEmitter, Input, Output } from '@angular/core'

@Component({
  selector: 'app-order-summary-info',
  templateUrl: './order-summary-info.component.html',
  styleUrls: ['./order-summary-info.component.scss']
})
export class OrderSummaryInfoComponent {
  @Input() totalPrice: number
  @Input() lastStep: boolean
  @Output() continueProcess = new EventEmitter()

  onNavigate (): void {
    this.continueProcess.emit()
  }
}
