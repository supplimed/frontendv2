import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { OrderSummaryInfoComponent } from './order-summary-info.component'

describe('OrderSummaryInfoComponent', () => {
  let component: OrderSummaryInfoComponent
  let fixture: ComponentFixture<OrderSummaryInfoComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrderSummaryInfoComponent]
    })
      .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderSummaryInfoComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
