import { Component, Input, OnChanges, SimpleChanges } from '@angular/core'
import { PaginationI } from '../../models/product.data'
import { ProductService } from '../../services/product.service'

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss']
})
export class PaginatorComponent implements OnChanges {
  @Input() pagination: PaginationI
  public firstPageIndication: number
  public lastPageIndication: number
  public pageSize: number

  constructor (private productService: ProductService) { }

  ngOnChanges (changes: SimpleChanges): void{
    if (changes.pagination.currentValue) {
      this.firstPageIndication = this.pagination.page * this.pagination.pageSize - this.pagination.pageSize + 1

      this.pagination.page === this.pagination.pageCount
        ? this.lastPageIndication = this.pagination.totalCount
        : this.lastPageIndication = this.pagination.page * this.pagination.pageSize

      this.pageSize = this.pagination.pageSize
    }
  }

  public onPreviousPage (): void{
    this.pagination.page === 1
      ? this.productService.getProducts(1, this.pagination.pageSize).subscribe()
      : this.productService.getProducts(this.pagination.page - 1, this.pagination.pageSize).subscribe()
  }

  public onNextPage (): void{
    this.pagination.page === this.pagination.pageCount
      ? this.productService.getProducts(this.pagination.pageCount, this.pagination.pageSize).subscribe()
      : this.productService.getProducts(this.pagination.page + 1, this.pagination.pageSize).subscribe()
  }

  public updatePageSize (newPageSize: number): void {
    this.productService.getProducts(1, newPageSize).subscribe()
  }
}
