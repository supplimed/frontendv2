import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { OrderAddressesInfoComponent } from './order-addresses-info.component'
import { AddressElementComponent } from '../address-element/address-element.component'

describe('OrderInfoComponent', () => {
  let component: OrderAddressesInfoComponent
  let fixture: ComponentFixture<OrderAddressesInfoComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        OrderAddressesInfoComponent,
        AddressElementComponent
      ]
    })
      .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderAddressesInfoComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
