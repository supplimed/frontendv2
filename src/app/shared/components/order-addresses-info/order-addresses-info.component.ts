import { Component, Input } from '@angular/core'

@Component({
  selector: 'app-order-addresses-info',
  templateUrl: './order-addresses-info.component.html',
  styleUrls: ['./order-addresses-info.component.scss']
})

export class OrderAddressesInfoComponent {
  @Input() deliveryAddress
  @Input() billingAddress
}
