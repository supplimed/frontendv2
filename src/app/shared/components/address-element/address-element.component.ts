import { Component, Input } from '@angular/core'
import { AddressI } from '../../models/address.data'

@Component({
  selector: 'app-address-element',
  templateUrl: './address-element.component.html',
  styleUrls: ['./address-element.component.scss']
})
export class AddressElementComponent {
  @Input() address: AddressI
}
