import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { CreditCardSelectionComponent } from './credit-card-selection.component'
import { MAT_DIALOG_DATA, MatDialogModule, MatDialogRef } from '@angular/material/dialog'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { MatFormFieldModule } from '@angular/material/form-field'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { CdkScrollableModule, ScrollingModule } from '@angular/cdk/scrolling'

describe('CreditCardSelectionComponent', () => {
  let component: CreditCardSelectionComponent
  let fixture: ComponentFixture<CreditCardSelectionComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreditCardSelectionComponent],
      imports: [
        MatDialogModule,
        ReactiveFormsModule,
        FormsModule,
        MatFormFieldModule,
        HttpClientTestingModule,
        CdkScrollableModule,
        ScrollingModule,
        MatDialogModule
      ],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: { cards: { expirationDate: '', alias: '', cardType: '', id: '' }, selectedCard: 0 } },
        { provide: MatDialogRef, useValue: {} }
      ]
    })
      .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditCardSelectionComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
