import { Component, Inject, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog'
import {
  MangoPayCardFormI,
  MangoPayRegisteredCardI
} from '../../models/mangoPay.data'

interface Month {
  value: string
}

interface Year {
  value: string
}

const EXPIRATION_YEARS = 20

@Component({
  selector: 'app-credit-card-selection',
  templateUrl: './credit-card-selection.component.html',
  styleUrls: ['./credit-card-selection.component.scss']
})
export class CreditCardSelectionComponent implements OnInit {
  cardForm: FormGroup
  selectedCard: number
  showSelectCreditCard: boolean
  selectedCardValue: MangoPayRegisteredCardI

  months: Month[] = [
    { value : '01' },
    { value : '02' },
    { value : '03' },
    { value : '04' },
    { value : '05' },
    { value : '06' },
    { value : '07' },
    { value : '08' },
    { value : '09' },
    { value : '10' },
    { value : '11' },
    { value : '12' }
  ]

  years: Year[] = [];

  constructor (@Inject(MAT_DIALOG_DATA) public data: { cards: MangoPayRegisteredCardI[], selectedCard: number},
    private dialogRef: MatDialogRef<CreditCardSelectionComponent>,
    private formBuilder: FormBuilder) {
  }

  ngOnInit (): void {
    this.cardForm = this.formBuilder.group({
      cardNumber: ['', [Validators.required]],
      cardExpirationMonthDate: ['', [Validators.required]],
      cardExpirationYearDate: ['', [Validators.required]],
      cardCvx: ['', [Validators.required]]
    })
    this.showSelectCreditCard = true
    this.selectedCard = this.data.selectedCard
    this.selectedCardValue = this.data.cards[this.selectedCard]

    // Creation of years
    let currentYear: number = new Date().getFullYear()
    currentYear = currentYear - 2000
    for (let i = 0; i <= EXPIRATION_YEARS; i++) {
      const temp = currentYear + i
      this.years.push({ value: temp.toString() })
    }
  }

  setSelectCard (id: number, cardValue: MangoPayRegisteredCardI): void {
    this.selectedCard = id
    this.selectedCardValue = cardValue
  }

  onSubmit ({ value }: {value: MangoPayCardFormI}): void {
    this.dialogRef.close({ newCard: value, event: 'add' })
  }

  setShowSelectCreditCard (): void {
    this.showSelectCreditCard = !this.showSelectCreditCard
  }
}
