/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { ProductCardComponent } from './product-card.component'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { CURRENCIES, ProductData } from '../../models/product.data'

const mockProduct: ProductData = {
  id: '0',
  name: 'Name for 0',
  price: 0,
  currency: CURRENCIES.EURO,
  description: 'Description for 0',
  imageUrl: 'assets/images/potaras.png',
  type: 'product'
}

describe('ProductCardComponent', () => {
  let component: ProductCardComponent
  let fixture: ComponentFixture<ProductCardComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProductCardComponent],
      imports: [HttpClientTestingModule]
    })
      .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductCardComponent)
    component = fixture.componentInstance
    component.productData = new ProductData(mockProduct)
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
