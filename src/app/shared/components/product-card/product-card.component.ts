import { Component, Input } from '@angular/core'
import { ProductData } from '../../models/product.data'
import { BasketService } from '../../services/basket.service'

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss']
})
export class ProductCardComponent {
  @Input() productData: ProductData

  constructor (private basketService: BasketService) {
  }

  public addProduct ($event): void{
    $event.stopPropagation()

    const products = [{
      id: this.productData.id,
      quantity: 1
    }]
    this.basketService.addBasket(products).subscribe()
  }
}
