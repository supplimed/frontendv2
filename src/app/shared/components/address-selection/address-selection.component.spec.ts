import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { AddressSelectionComponent } from './address-selection.component'
import { MAT_DIALOG_DATA, MatDialogModule, MatDialogRef } from '@angular/material/dialog'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { MatFormFieldModule } from '@angular/material/form-field'
import { AddressElementComponent } from '../address-element/address-element.component'
import { CdkScrollableModule, ScrollingModule } from '@angular/cdk/scrolling'

describe('AddressSelectionComponent', () => {
  let component: AddressSelectionComponent
  let fixture: ComponentFixture<AddressSelectionComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AddressSelectionComponent,
        AddressElementComponent
      ],
      imports: [
        ReactiveFormsModule,
        FormsModule,
        MatFormFieldModule,
        HttpClientTestingModule,
        CdkScrollableModule,
        ScrollingModule,
        MatDialogModule
      ],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: {} },
        { provide: MatDialogRef, useValue: {} }
      ]
    })
      .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(AddressSelectionComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
