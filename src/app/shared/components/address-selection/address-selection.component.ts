import { Component, Inject, OnInit } from '@angular/core'
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog'
import { AddressI } from '../../models/address.data'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'

enum AddressType {
  delivery,
  billing
}

@Component({
  selector: 'app-address-selection',
  templateUrl: './address-selection.component.html',
  styleUrls: ['./address-selection.component.scss']
})
export class AddressSelectionComponent implements OnInit {
  selectedAddress: number
  selectedAddressValue: AddressI
  title: string
  deliveryAddressForm: FormGroup
  showSelectAddress: boolean

  constructor (@Inject(MAT_DIALOG_DATA) public data: { addresses: AddressI[], type: AddressType, selectedAddress: number},
               private dialogRef: MatDialogRef<AddressSelectionComponent>,
               private formBuilder: FormBuilder) {
    this.deliveryAddressForm = this.formBuilder.group({
      address: ['', [Validators.required]],
      postalCode: ['', [Validators.required]],
      city: ['', [Validators.required]]
    })
  }

  ngOnInit (): void {
    this.data.type === AddressType.delivery ? this.title = 'livraison' : this.title = 'facturation'
    this.showSelectAddress = true
    this.selectedAddress = this.data.selectedAddress
  }

  setSelectedDeliveryAddress (index: number, address: AddressI): void {
    this.selectedAddress = index
    this.selectedAddressValue = address
  }

  onAddAddress ({ value }: {value: AddressI}): void {
    this.dialogRef.close({ newAddress: value, event: 'add' })
  }

  setShowSelectAddress (): void {
    this.showSelectAddress = !this.showSelectAddress
  }
}
