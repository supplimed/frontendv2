import { NgModule } from '@angular/core'
import { HeaderComponent } from './header/header.component'
import { ProductCardComponent } from './product-card/product-card.component'
import { BrowserModule } from '@angular/platform-browser'
import { AppRoutingModule } from '../../app-routing.module'
import { PaginatorComponent } from './paginator/paginator.component'
import { BasketElementComponent } from './basket-element/basket-element.component'
import { MatIconModule } from '@angular/material/icon'
import { HttpClientModule } from '@angular/common/http'
import { MatBadgeModule } from '@angular/material/badge'
import { MatSelectModule } from '@angular/material/select'
import { MatRippleModule } from '@angular/material/core'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { MatButtonModule } from '@angular/material/button'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { RegisteredCardComponent } from './registered-card/registered-card.component'
import { CardDatePipe } from '../core/cardDate.pipe'
import { CardAlliasPipe } from '../core/cardAllias.pipe'
import { AddressElementComponent } from './address-element/address-element.component'
import { MatCheckboxModule } from '@angular/material/checkbox'
import { OrderAddressesInfoComponent } from './order-addresses-info/order-addresses-info.component'
import { OrderSummaryInfoComponent } from './order-summary-info/order-summary-info.component'
import { OrderHistorySummaryComponent } from './order-history-summary/order-history-summary.component'
import { AddressSelectionComponent } from './address-selection/address-selection.component'
import { MatDialogModule } from '@angular/material/dialog'
import { MatFormFieldModule } from '@angular/material/form-field'
import { MatInputModule } from '@angular/material/input'
import { CreditCardSelectionComponent } from './credit-card-selection/credit-card-selection.component'
import { ScrollingModule } from '@angular/cdk/scrolling'

@NgModule({
  declarations: [
    ProductCardComponent,
    PaginatorComponent,
    BasketElementComponent,
    HeaderComponent,
    AddressElementComponent,
    RegisteredCardComponent,
    OrderAddressesInfoComponent,
    OrderSummaryInfoComponent,
    OrderHistorySummaryComponent,
    CardDatePipe,
    CardAlliasPipe,
    AddressSelectionComponent,
    CreditCardSelectionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatIconModule,
    HttpClientModule,
    MatBadgeModule,
    MatSelectModule,

    MatRippleModule,
    BrowserAnimationsModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatCheckboxModule,
    MatDialogModule,
    MatFormFieldModule,
    FormsModule,
    MatInputModule,
    MatRippleModule,
    ScrollingModule
  ],
  exports: [
    RegisteredCardComponent,
    ProductCardComponent,
    PaginatorComponent,
    BasketElementComponent,
    HeaderComponent,
    OrderAddressesInfoComponent,
    OrderSummaryInfoComponent,
    OrderHistorySummaryComponent,
    CardDatePipe,
    CardAlliasPipe,
    AddressElementComponent,
    AddressSelectionComponent,
    CreditCardSelectionComponent
  ]
})
export class ComponentsModule { }
