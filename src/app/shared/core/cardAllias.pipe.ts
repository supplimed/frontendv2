import { Pipe, PipeTransform } from '@angular/core'

@Pipe({ name: 'cardAllias' })
export class CardAlliasPipe implements PipeTransform {
  transform (value: string): string {
    return '•••• •••• •••• ' + value.slice(12, 16)
  }
}
