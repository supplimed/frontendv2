import { Pipe, PipeTransform } from '@angular/core'

@Pipe({ name: 'cardDate' })
export class CardDatePipe implements PipeTransform {
  transform (value: string): string {
    return value.slice(0, 2) + '/' + value.slice(2, 4)
  }
}
