import { Component, OnInit } from '@angular/core'
import { AccountService } from './shared/services/account.service'
import { BasketService } from './shared/services/basket.service'
import { IconService } from './shared/services/icon.service'
import { registerLocaleData } from '@angular/common'
import localeFr from '@angular/common/locales/fr'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  constructor (private accountService: AccountService,
               private basketService: BasketService,
               private iconService: IconService) {
  }

  ngOnInit (): void {
    if (this.accountService.getCurrentUser()) {
      this.basketService.getBasket().subscribe()
    }
    // set Local Data for currency
    registerLocaleData(localeFr, 'fr')
  }
}
