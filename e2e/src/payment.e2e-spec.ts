import { browser, logging } from 'protractor'
import { CommonPo } from './common.po'
// import { PaymentPagePo } from './payment.po'
// import { LogInPagePo } from './account.po'

fdescribe('payment tests', () => {
  // let paymentPage: PaymentPagePo
  // let logInPage: LogInPagePo
  let common: CommonPo

  beforeEach(() => {
    // paymentPage = new PaymentPagePo()
    // logInPage = new LogInPagePo()
    common = new CommonPo()

    common.clearDB()
  })

  // TODO IN US REFACTO CORNER CASE

  /* it('should payment be successful', () => {
    logInPage.logInGogeta()
    browser.waitForAngular()
    paymentPage.navigateTo()
    paymentPage.getCardContainer().click()
    paymentPage.getRegisteredCard('1').click()
    paymentPage.getPayButtonId().click()
    paymentPage.getPayButton().click()
    browser.waitForAngular()

    expect(browser.getCurrentUrl()).toEqual(browser.baseUrl + 'payment/accepted')
  }) */

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER)
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE
    } as logging.Entry))
  })
})
