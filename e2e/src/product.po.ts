import { browser, by, element, ElementFinder } from 'protractor'

export class ProductFocusPagePo {
  navigateTo (id: string): Promise<unknown> {
    return browser.get(browser.baseUrl + 'products/' + id) as Promise<unknown>
  }

  getProductFocusId (): ElementFinder {
    return element(by.css('.productFocusId'))
  }

  getProductNotFoundMessage (): ElementFinder {
    return element(by.css('.productNotFoundMessage'))
  }

  getPlusButton (): ElementFinder {
    return element(by.css('.productFocusPlus'))
  }

  getMinusButton (): ElementFinder {
    return element(by.css('.productFocusMinus'))
  }

  getProductQuantity (): ElementFinder {
    return element(by.css('.productFocusQuantity'))
  }

  getProductTotalPrice (): ElementFinder {
    return element(by.css('.productFocusTotalPrice'))
  }

  getAddToBasketButton (): ElementFinder {
    return element(by.css('.productFocusAddButton'))
  }
}

export class ProductPagePo {
  navigateTo (): Promise<unknown> {
    return browser.get(browser.baseUrl + 'products') as Promise<unknown>
  }

  getProductById (id: string): ElementFinder {
    return element(by.css('.product' + id))
  }

  getNameOfProductById (id: string): ElementFinder {
    return element(by.css('.product' + id)).element(by.css('.productCardName'))
  }

  getPreviousPage (): ElementFinder {
    return element(by.css('.previousPageBtn'))
  }

  getNextPage (): ElementFinder {
    return element(by.css('.nextPageBtn'))
  }

  setPageSize50 (): void {
    element(by.css('.pageSizeSelector')).click().then(() => {
      element(by.cssContainingText('mat-option', '50')).click()
    })
  }

  setPageSize20 (): void {
    element(by.css('.pageSizeSelector')).click().then(() => {
      element(by.cssContainingText('mat-option', '20')).click()
    })
  }

  getPageSizeSelectorValue () {
    return element(by.css('.pageSizeSelectorValue'))
  }

  getPageIndication (): ElementFinder {
    return element(by.css('.pageIndicationLabel'))
  }

  getAddButtonOfProductById (id: string): ElementFinder {
    return element(by.css('.product' + id)).element(by.css('.productCardAddButton'))
  }
}
