import { browser, by, element, ElementFinder } from 'protractor'
import { HeaderComponentPo } from './header.po'

export class BasketPagePo extends HeaderComponentPo {
  navigateTo (): Promise<unknown> {
    return browser.get(browser.baseUrl + 'basket/summary') as Promise<unknown>
  }

  getBasketElementById (id: string): ElementFinder {
    return element(by.css('.basketElement' + id)).element(by.css('.container'))
  }

  getBasketElementId (id: string): ElementFinder {
    return element(by.css('.basketElement' + id)).element(by.css('.basketElementId'))
  }

  getBasketElementIdPrice (id: string): ElementFinder {
    return element(by.css('.basketElement' + id)).element(by.css('.unitPrice'))
  }

  getBasketElementIdQuantity (id: string): ElementFinder {
    return element(by.css('.basketElement' + id)).element(by.css('.basketElementQuantity'))
  }

  getBasketElementIdBulkPrice (id: string): ElementFinder {
    return element(by.css('.basketElement' + id)).element(by.css('.bulkPrice'))
  }

  getBasketElementMinusButton (id: string): ElementFinder {
    return element(by.css('.basketElement' + id)).element(by.css('.minusBtn'))
  }

  getBasketElementPlusButton (id: string): ElementFinder {
    return element(by.css('.basketElement' + id)).element(by.css('.plusBtn'))
  }

  getBasketElementClearButton (id: string): ElementFinder {
    return element(by.css('.basketElement' + id)).element(by.css('.clearBtn'))
  }

  getBasketOrderButton (): ElementFinder {
    return element(by.css('.summaryOrderBtn'))
  }

  checkBasketElementWithIdAndQuantity (id: string, quantity: number): void {
    const productId: string = (+id + 1).toString()
    expect(this.getBasketElementId(id).getText()).toEqual('ref: ' + productId)
    expect(this.getBasketElementIdPrice(id).getText()).toEqual('Prix unitaire: ' + productId + ',00 €')
    expect(this.getBasketElementIdQuantity(id).getText()).toEqual(quantity.toString())
    expect(this.getBasketElementIdBulkPrice(id).getText()).toEqual((quantity * +productId).toString() + ',00 €')
  }

  getBasketTotalPrice (): ElementFinder {
    return element(by.css('.summaryTotalPriceValue'))
  }
}
