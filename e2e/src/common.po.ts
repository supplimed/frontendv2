import { protractor, promise } from 'protractor'
import * as http from 'http'

export class CommonPo {
  clearDB (): promise.Promise<unknown> {
    const deferred = protractor.promise.defer()

    const options = {
      hostname: 'localhost',
      port: 3000,
      path: '/test/reset',
      method: 'GET'
    }

    const callback = function (): void {
      deferred.fulfill()
    }

    const req = http.request(options, callback)
    req.end()

    return deferred.promise
  };
}
