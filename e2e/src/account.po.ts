import { browser, by, element, ElementFinder } from 'protractor'
import { HeaderComponentPo } from './header.po'

export class LogInPagePo {
  navigateTo (): Promise<unknown> {
    return browser.get(browser.baseUrl + 'login') as Promise<unknown>
  }

  getEmailField (): ElementFinder {
    return element(by.css('.email'))
  }

  getPasswordField (): ElementFinder {
    return element(by.css('.password'))
  }

  getSubmitButton (): ElementFinder {
    return element(by.css('.submit'))
  }

  logInGogeta (): void {
    this.navigateTo()
    this.getEmailField().sendKeys('gogeta.denamek@dbz.com')
    this.getPasswordField().sendKeys('supersayan')
    this.getSubmitButton().click()
  }

  logInBoo (): void {
    this.navigateTo()
    this.getEmailField().sendKeys('booboo@dbz.com')
    this.getPasswordField().sendKeys('babidi boo')
    this.getSubmitButton().click()
  }
}

export class SignUpPagePo {
  navigateTo (): Promise<unknown> {
    return browser.get(browser.baseUrl + 'signup') as Promise<unknown>
  }

  getEmailField (): ElementFinder {
    return element(by.css('.email'))
  }

  getFirstNameField (): ElementFinder {
    return element(by.css('.firstName'))
  }

  getLastNameField (): ElementFinder {
    return element(by.css('.lastName'))
  }

  getPasswordField (): ElementFinder {
    return element(by.css('.password'))
  }

  getPasswordConfirmationField (): ElementFinder {
    return element(by.css('.passwordConfirmation'))
  }

  getSubmitButton (): ElementFinder {
    return element(by.css('.submit'))
  }

  getErrorMessage (): ElementFinder {
    return element(by.css('.signUpErrorMessage'))
  }

  signUpBroly (): void {
    this.navigateTo()
    this.getFirstNameField().sendKeys('Broly')
    this.getLastNameField().sendKeys('DeVegeta')
    this.getEmailField().sendKeys('broly@dbz.com')
    this.getPasswordField().sendKeys('supersayan')
    this.getPasswordConfirmationField().sendKeys('supersayan')
    this.getSubmitButton().click()
  }
}

export class ProfilePagePo extends HeaderComponentPo {
  navigateTo (): Promise<unknown> {
    return browser.get(browser.baseUrl + 'account/profile') as Promise<unknown>
  }

  getProfileFirstName (): ElementFinder {
    return element(by.css('.profileFirstName'))
  }

  getProfileLastName (): ElementFinder {
    return element(by.css('.profileLastName'))
  }

  getProfileEmail (): ElementFinder {
    return element(by.css('.profileEmail'))
  }

  getDisconnectLink (): ElementFinder {
    return element(by.css('.disconnectBtn'))
  }

  getOrderHistorySummaryLink (): ElementFinder {
    return element(by.css('.orderHistoryBtn'))
  }

  disconnect (): void {
    this.navigateTo()
    this.getDisconnectLink().click()
  }
}
