import { browser, by, element, ElementFinder } from 'protractor'

export class HeaderComponentPo {
  navigateTo (): Promise<unknown> {
    return browser.get(browser.baseUrl) as Promise<unknown>
  }

  getHomeLink (): ElementFinder {
    return element(by.css('.homeLink'))
  }

  getLogInLink (): ElementFinder {
    return element(by.css('.logInLink'))
  }

  getSignUpLink (): ElementFinder {
    return element(by.css('.signUpLink'))
  }

  getBasketLink (): ElementFinder {
    return element(by.css('.basketLink'))
  }

  getUserFirstName (): ElementFinder {
    return element(by.css('.profileLinkValue'))
  }

  getProductQuantity (): ElementFinder {
    return element(by.css('.mat-badge-content'))
  }

  checkHeaderWhenConnected (): void {
    expect(browser.isElementPresent(this.getHomeLink())).toBeTruthy()
    expect(browser.isElementPresent(this.getBasketLink())).toBeTruthy()
    expect(browser.isElementPresent(this.getUserFirstName())).toBeTruthy()
    expect(browser.isElementPresent(this.getLogInLink())).toBeFalsy()
    expect(browser.isElementPresent(this.getSignUpLink())).toBeFalsy()
    expect(this.getProductQuantity().getText()).toEqual('3')
    expect(this.getUserFirstName().getText()).toEqual('Gogeta')
  }

  checkHeaderWhenDisconnected (): void {
    expect(browser.isElementPresent(this.getHomeLink())).toBeTruthy()
    expect(browser.isElementPresent(this.getLogInLink())).toBeTruthy()
    expect(browser.isElementPresent(this.getSignUpLink())).toBeTruthy()
    expect(browser.isElementPresent(this.getBasketLink())).toBeFalsy()
    expect(browser.isElementPresent(this.getUserFirstName())).toBeFalsy()
  }
}
