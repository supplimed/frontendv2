import { LogInPagePo, ProfilePagePo } from './account.po'
import { CommonPo } from './common.po'
import { browser, logging } from 'protractor'
import { OrderHistoryFocusPagePo, OrderHistorySummaryPagePo } from './order-history.po'
import { BasketPagePo } from './basket.po'

describe('OrderHistorySummary tests', () => {
  let logInPage: LogInPagePo
  let profilePage: ProfilePagePo
  let orderHistorySummaryPage: OrderHistorySummaryPagePo
  let common: CommonPo

  beforeEach(() => {
    logInPage = new LogInPagePo()
    orderHistorySummaryPage = new OrderHistorySummaryPagePo()
    profilePage = new ProfilePagePo()
    common = new CommonPo()

    common.clearDB()
  })

  it('should display order history summary correctly', () => {
    logInPage.logInGogeta()
    browser.waitForAngular()
    profilePage.navigateTo()
    profilePage.getOrderHistorySummaryLink().click()

    expect(orderHistorySummaryPage.getOrderId('0').getText()).toEqual('1111-111111-1111')
    expect(orderHistorySummaryPage.getDate('0').getText()).toEqual('samedi 11 juillet 2020')
    expect(orderHistorySummaryPage.getTotalPrice('0').getText()).toEqual('5,00 €')
    expect(browser.getCurrentUrl()).toEqual(browser.baseUrl + 'account/order/history')
  })

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER)
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE
    } as logging.Entry))
  })
})

describe('OrderHistorySummary tests', () => {
  let logInPage: LogInPagePo
  let profilePage: ProfilePagePo
  let orderHistorySummaryPage: OrderHistorySummaryPagePo
  let common: CommonPo

  beforeEach(() => {
    logInPage = new LogInPagePo()
    orderHistorySummaryPage = new OrderHistorySummaryPagePo()
    profilePage = new ProfilePagePo()
    common = new CommonPo()

    common.clearDB()
  })

  it('should display order history summary correctly', () => {
    logInPage.logInGogeta()
    browser.waitForAngular()
    profilePage.navigateTo()
    profilePage.getOrderHistorySummaryLink().click()

    expect(orderHistorySummaryPage.getOrderId('0').getText()).toEqual('1111-111111-1111')
    expect(orderHistorySummaryPage.getDate('0').getText()).toEqual('samedi 11 juillet 2020')
    expect(orderHistorySummaryPage.getTotalPrice('0').getText()).toEqual('5,00 €')
    expect(browser.getCurrentUrl()).toEqual(browser.baseUrl + 'account/order/history')
  })

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER)
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE
    } as logging.Entry))
  })
})

describe('OrderHistoryFocus tests', () => {
  let logInPage: LogInPagePo
  let profilePage: ProfilePagePo
  let orderHistorySummaryPage: OrderHistorySummaryPagePo
  let orderHistoryFocusPage: OrderHistoryFocusPagePo
  let basketPage: BasketPagePo
  let common: CommonPo

  beforeEach(() => {
    logInPage = new LogInPagePo()
    orderHistorySummaryPage = new OrderHistorySummaryPagePo()
    orderHistoryFocusPage = new OrderHistoryFocusPagePo()
    profilePage = new ProfilePagePo()
    basketPage = new BasketPagePo()
    common = new CommonPo()

    common.clearDB()
  })

  it('should display order history focus correctly', () => {
    logInPage.logInGogeta()
    browser.waitForAngular()
    profilePage.navigateTo()
    profilePage.getOrderHistorySummaryLink().click()
    orderHistorySummaryPage.getHistoryOrderLink('0').click()
    browser.waitForAngular()

    expect(browser.getCurrentUrl()).toEqual(browser.baseUrl + 'order/history/1111-111111-1111')
    expect(orderHistoryFocusPage.getOrderId().getText()).toEqual('1111-111111-1111')
    expect(orderHistoryFocusPage.getTotalPrice().getText()).toEqual('5,00 €')
    expect(orderHistoryFocusPage.getDate().getText()).toEqual('samedi 11 juillet 2020')

    expect(orderHistoryFocusPage.getDeliveryAddressAddress().getText()).toEqual('1, rue du palmier')
    expect(orderHistoryFocusPage.getDeliveryAddressCity().getText()).toEqual('ile de tortue génial')
    expect(orderHistoryFocusPage.getDeliveryAddressPostalCode().getText()).toEqual('11111')

    expect(orderHistoryFocusPage.getBillingAddressAddress().getText()).toEqual('2, rue du palmier')
    expect(orderHistoryFocusPage.getBillingAddressCity().getText()).toEqual('ile de tortue génial')
    expect(orderHistoryFocusPage.getBillingAddressPostalCode().getText()).toEqual('22222')

    basketPage.checkBasketElementWithIdAndQuantity('0', 1)
    basketPage.checkBasketElementWithIdAndQuantity('1', 2)

    browser.manage().logs().get(logging.Type.BROWSER).catch()
  })

  it('should display order history after clicking on backToOrderHistoryBtn', () => {
    logInPage.logInGogeta()
    browser.waitForAngular()
    profilePage.navigateTo()
    profilePage.getOrderHistorySummaryLink().click()
    orderHistorySummaryPage.getHistoryOrderLink('0').click()
    browser.waitForAngular()

    expect(browser.getCurrentUrl()).toEqual(browser.baseUrl + 'order/history/1111-111111-1111')
    orderHistoryFocusPage.getBackToHistoryBtn().click()
    expect(browser.getCurrentUrl()).toEqual(browser.baseUrl + 'account/order/history')
    browser.manage().logs().get(logging.Type.BROWSER).catch()
  })

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER)
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE
    } as logging.Entry))
  })
})
