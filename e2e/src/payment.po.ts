import { browser, by, element, ElementFinder } from 'protractor'

export class PaymentPagePo {
  navigateTo (): Promise<unknown> {
    return browser.get(browser.baseUrl + 'basket/payment') as Promise<unknown>
  }

  getRegisteredCard (id: string): ElementFinder {
    return element(by.css('.cardElement' + id))
  }

  getPayButton (): ElementFinder {
    return element(by.css('.validateBtn'))
  }

  getPayButtonId (): ElementFinder {
    return element(by.id('validateBtn'))
  }

  getCardContainer (): ElementFinder {
    return element(by.css('.cardContainer'))
  }
}
