import { browser, by, element, ElementFinder } from 'protractor'

export class OrderHistorySummaryPagePo {
  navigateTo (): Promise<unknown> {
    return browser.get(browser.baseUrl + 'order/history') as Promise<unknown>
  }

  getHistoryOrderLink (id: string): ElementFinder {
    return element(by.css('.orderHistory' + id))
  }

  getOrderId (id: string): ElementFinder {
    return element(by.css('.orderHistory' + id)).element(by.css('.orderId'))
  }

  getDate (id: string): ElementFinder {
    return element(by.css('.orderHistory' + id)).element(by.css('.date'))
  }

  getTotalPrice (id: string): ElementFinder {
    return element(by.css('.orderHistory' + id)).element(by.css('.totalPrice'))
  }
}

export class OrderHistoryFocusPagePo {
  getOrderId (): ElementFinder {
    return element(by.css('.orderHistoryId'))
  }

  getDate (): ElementFinder {
    return element(by.css('.orderHistoryDate'))
  }

  getTotalPrice (): ElementFinder {
    return element(by.css('.summaryTotalPriceValue'))
  }

  getDeliveryAddressAddress (): ElementFinder {
    return element(by.css('.deliveryAddress')).element(by.css('.address'))
  }

  getDeliveryAddressCity (): ElementFinder {
    return element(by.css('.deliveryAddress')).element(by.css('.city'))
  }

  getDeliveryAddressPostalCode (): ElementFinder {
    return element(by.css('.deliveryAddress')).element(by.css('.postalCode'))
  }

  getBillingAddressAddress (): ElementFinder {
    return element(by.css('.billingAddress')).element(by.css('.address'))
  }

  getBillingAddressCity (): ElementFinder {
    return element(by.css('.billingAddress')).element(by.css('.city'))
  }

  getBillingAddressPostalCode (): ElementFinder {
    return element(by.css('.billingAddress')).element(by.css('.postalCode'))
  }

  getBackToHistoryBtn (): ElementFinder {
    return element(by.css('.backToHistoryBtn'))
  }
}
