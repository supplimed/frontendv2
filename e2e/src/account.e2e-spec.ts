import { LogInPagePo, ProfilePagePo, SignUpPagePo } from './account.po'
import { browser, logging } from 'protractor'
import { HeaderComponentPo } from './header.po'
import { CommonPo } from './common.po'

describe('logIn tests', () => {
  let logInPage: LogInPagePo
  let header: HeaderComponentPo
  let profilePage: ProfilePagePo
  let common: CommonPo

  beforeEach(() => {
    logInPage = new LogInPagePo()
    header = new HeaderComponentPo()
    common = new CommonPo()
    profilePage = new ProfilePagePo()

    common.clearDB()
  })

  it('should display correct links if the user is disconnected', () => {
    header.navigateTo()
    header.checkHeaderWhenDisconnected()
  })

  it('should login correctly', () => {
    logInPage.navigateTo()
    expect(logInPage.getSubmitButton().isEnabled()).toBeFalsy()
    logInPage.getEmailField().sendKeys('gogeta.denamek@dbz.com')
    logInPage.getPasswordField().sendKeys('supersayan')
    expect(logInPage.getSubmitButton().isEnabled()).toBeTruthy()
    logInPage.getSubmitButton().click()
    browser.waitForAngular()

    expect(browser.getCurrentUrl()).toEqual(browser.baseUrl)
    header.checkHeaderWhenConnected()
    profilePage.disconnect()
  })

  it('should logout correctly', () => {
    logInPage.logInGogeta()

    header.getBasketLink().click()
    profilePage.disconnect()
    expect(browser.getCurrentUrl()).toEqual(browser.baseUrl)
    header.checkHeaderWhenDisconnected()
  })

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER)
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE
    } as logging.Entry))
  })
})

describe('signUp tests', () => {
  let signUpPage: SignUpPagePo
  let profilePage: ProfilePagePo

  beforeEach(() => {
    signUpPage = new SignUpPagePo()
    profilePage = new ProfilePagePo()
  })

  it('should sign up correctly', () => {
    signUpPage.signUpBroly()

    expect(browser.getCurrentUrl()).toEqual(browser.baseUrl)
    profilePage.disconnect()
  })

  it('should not sign up twice correctly', () => {
    signUpPage.signUpBroly()
    signUpPage.signUpBroly()

    expect(browser.isElementPresent(signUpPage.getErrorMessage())).toBeTruthy()
    browser.manage().logs().get(logging.Type.BROWSER).catch()
  })

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER)
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE
    } as logging.Entry))
  })
})

describe('profile tests', () => {
  let logInPage: LogInPagePo
  let profilePage: ProfilePagePo

  beforeEach(() => {
    logInPage = new LogInPagePo()
    profilePage = new ProfilePagePo()
  })

  it('should display profile correctly', () => {
    logInPage.logInGogeta()
    profilePage.getUserFirstName().click()

    expect(profilePage.getProfileFirstName().getAttribute('formcontrolname')).toEqual('firstName')
    expect(profilePage.getProfileLastName().getAttribute('formcontrolname')).toEqual('lastName')
    expect(profilePage.getProfileEmail().getAttribute('formcontrolname')).toEqual('email')
  })

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER)
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE
    } as logging.Entry))
  })
})
