import { LogInPagePo, ProfilePagePo } from './account.po'
import { browser, logging } from 'protractor'
import { ProductFocusPagePo, ProductPagePo } from './product.po'
import { BasketPagePo } from './basket.po'
import { CommonPo } from './common.po'

describe('basket tests', () => {
  let logInPage: LogInPagePo
  let basketPage: BasketPagePo
  let productPage: ProductPagePo
  let productFocusPage: ProductFocusPagePo
  let profilePage: ProfilePagePo
  let common: CommonPo

  beforeEach(() => {
    logInPage = new LogInPagePo()
    basketPage = new BasketPagePo()
    productPage = new ProductPagePo()
    productFocusPage = new ProductFocusPagePo()
    profilePage = new ProfilePagePo()
    common = new CommonPo()

    common.clearDB()

    logInPage.logInGogeta()
    browser.waitForAngular()
  })

  it('should show basket', () => {
    basketPage.navigateTo()
    expect(browser.getCurrentUrl()).toEqual(browser.baseUrl + 'basket/summary')

    basketPage.checkBasketElementWithIdAndQuantity('0', 1)
    basketPage.checkBasketElementWithIdAndQuantity('1', 2)
    expect(basketPage.getBasketTotalPrice().getText()).toEqual('5,00 €')
  })

  it('should add product from products page for a connected a account', () => {
    productPage.navigateTo()
    productPage.getAddButtonOfProductById('3').click()

    basketPage.navigateTo()

    basketPage.checkBasketElementWithIdAndQuantity('2', 1)
    expect(basketPage.getBasketTotalPrice().getText()).toEqual('8,00 €')
  })

  it('should add product from products page focus for a connected a account', () => {
    productPage.navigateTo()
    productPage.getProductById('3').click()

    productFocusPage.getPlusButton().click()
    productFocusPage.getPlusButton().click()

    expect(productFocusPage.getProductQuantity().getText()).toEqual('3')
    expect(productFocusPage.getProductTotalPrice().getText()).toEqual('9,00 €')

    productFocusPage.getMinusButton().click()
    expect(productFocusPage.getProductQuantity().getText()).toEqual('2')
    expect(productFocusPage.getProductTotalPrice().getText()).toEqual('6,00 €')

    productFocusPage.getAddToBasketButton().click()
    basketPage.navigateTo()
    expect(browser.getCurrentUrl()).toEqual(browser.baseUrl + 'basket/summary')
    basketPage.checkBasketElementWithIdAndQuantity('2', 2)
    expect(basketPage.getBasketTotalPrice().getText()).toEqual('11,00 €')
    expect(basketPage.getProductQuantity().getText()).toEqual('5')
  })

  it('should add product from basket page focus for a connected a account', () => {
    basketPage.navigateTo()
    basketPage.getBasketElementPlusButton('1').click()
    basketPage.checkBasketElementWithIdAndQuantity('1', 3)
    expect(basketPage.getBasketTotalPrice().getText()).toEqual('7,00 €')
    expect(basketPage.getProductQuantity().getText()).toEqual('4')
  })

  it('should remove product from basket page focus for a connected a account', () => {
    basketPage.navigateTo()
    basketPage.getBasketElementMinusButton('1').click()
    basketPage.checkBasketElementWithIdAndQuantity('1', 1)
    expect(basketPage.getBasketTotalPrice().getText()).toEqual('3,00 €')
    expect(basketPage.getProductQuantity().getText()).toEqual('2')
  })

  it('should remove completely a product from basket page focus for a connected a account', () => {
    basketPage.navigateTo()
    basketPage.getBasketElementMinusButton('1').click()
    basketPage.getBasketElementMinusButton('1').click()
    expect(browser.isElementPresent(basketPage.getBasketElementId('1'))).toBeFalsy()
    expect(basketPage.getProductQuantity().getText()).toEqual('1')
  })

  it('should remove completely a product from basket page focus for a connected a account by clicking on clear', () => {
    basketPage.navigateTo()
    basketPage.getBasketElementClearButton('1').click()
    expect(browser.isElementPresent(basketPage.getBasketElementId('1'))).toBeFalsy()
    expect(basketPage.getProductQuantity().getText()).toEqual('1')
  })

  it('should display a focus product', () => {
    basketPage.navigateTo()
    browser.waitForAngular()
    basketPage.getBasketElementIdPrice('0').click()
    expect(browser.getCurrentUrl()).toEqual(browser.baseUrl + 'products/1')
    expect(productFocusPage.getProductFocusId().getText()).toEqual('ref: 1')
    expect(browser.isElementPresent(productFocusPage.getProductNotFoundMessage())).toBeFalsy()
  })

  it('should navigate to delivery page', () => {
    basketPage.navigateTo()
    basketPage.getBasketOrderButton().click()
    expect(browser.getCurrentUrl()).toEqual(browser.baseUrl + 'basket/delivery')
  })

  it('should navigate to forms profile page', () => {
    profilePage.disconnect()
    logInPage.logInBoo()
    browser.waitForAngular()
    basketPage.navigateTo()
    basketPage.getBasketOrderButton().click()
    expect(browser.getCurrentUrl()).toEqual(browser.baseUrl + 'basket/profile')
  })

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER)
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE
    } as logging.Entry))
  })
})
