import { browser, logging } from 'protractor'
import { ProductFocusPagePo, ProductPagePo } from './product.po'
import { CommonPo } from './common.po'

describe('products tests', () => {
  let productPage: ProductPagePo
  let productFocusPage: ProductFocusPagePo
  let common: CommonPo

  beforeEach(() => {
    productPage = new ProductPagePo()
    productFocusPage = new ProductFocusPagePo()
    common = new CommonPo()

    common.clearDB()
  })

  it('should display products the 20 first products', () => {
    productPage.navigateTo()

    expect(browser.isElementPresent(productPage.getProductById('0'))).toBeTruthy()
    expect(browser.isElementPresent(productPage.getProductById('19'))).toBeTruthy()
    expect(browser.isElementPresent(productPage.getProductById('20'))).toBeFalsy()
    expect(productPage.getNameOfProductById('0').getText()).toEqual('Name for 0')
    expect(productPage.getNameOfProductById('19').getText()).toEqual('Name for 19')
  })

  it('should display products from 21 to the 41 and then 0 to 20', () => {
    productPage.navigateTo()
    productPage.getNextPage().click()
    expect(browser.isElementPresent(productPage.getProductById('0'))).toBeTruthy()
    expect(browser.isElementPresent(productPage.getProductById('19'))).toBeTruthy()
    expect(browser.isElementPresent(productPage.getProductById('20'))).toBeFalsy()
    expect(productPage.getNameOfProductById('0').getText()).toEqual('Name for 20')
    expect(productPage.getNameOfProductById('19').getText()).toEqual('Name for 39')
    expect(productPage.getPageIndication().getText()).toEqual('21 - 40 sur 90')

    productPage.getPreviousPage().click()
    expect(browser.isElementPresent(productPage.getProductById('0'))).toBeTruthy()
    expect(browser.isElementPresent(productPage.getProductById('19'))).toBeTruthy()
    expect(browser.isElementPresent(productPage.getProductById('20'))).toBeFalsy()
    expect(productPage.getNameOfProductById('0').getText()).toEqual('Name for 0')
    expect(productPage.getNameOfProductById('19').getText()).toEqual('Name for 19')
    expect(productPage.getPageIndication().getText()).toEqual('1 - 20 sur 90')
  })

  it('should display products from 0 to 50 and then from 0 to 20', () => {
    productPage.navigateTo()
    productPage.setPageSize50()
    expect(browser.isElementPresent(productPage.getProductById('0'))).toBeTruthy()
    expect(browser.isElementPresent(productPage.getProductById('49'))).toBeTruthy()
    expect(browser.isElementPresent(productPage.getProductById('50'))).toBeFalsy()
    expect(productPage.getNameOfProductById('0').getText()).toEqual('Name for 0')
    expect(productPage.getNameOfProductById('49').getText()).toEqual('Name for 49')
    expect(productPage.getPageIndication().getText()).toEqual('1 - 50 sur 90')
    expect(productPage.getPageSizeSelectorValue().getText()).toEqual('50')

    productPage.setPageSize20()
    expect(browser.isElementPresent(productPage.getProductById('0'))).toBeTruthy()
    expect(browser.isElementPresent(productPage.getProductById('19'))).toBeTruthy()
    expect(browser.isElementPresent(productPage.getProductById('20'))).toBeFalsy()
    expect(productPage.getNameOfProductById('0').getText()).toEqual('Name for 0')
    expect(productPage.getNameOfProductById('19').getText()).toEqual('Name for 19')
    expect(productPage.getPageIndication().getText()).toEqual('1 - 20 sur 90')
    expect(productPage.getPageSizeSelectorValue().getText()).toEqual('20')
  })

  it('should display a focus product', () => {
    productPage.navigateTo()
    productPage.getProductById('0').click()
    expect(browser.getCurrentUrl()).toEqual(browser.baseUrl + 'products/0')
    expect(productFocusPage.getProductFocusId().getText()).toEqual('ref: 0')
    expect(browser.isElementPresent(productFocusPage.getProductNotFoundMessage())).toBeFalsy()
  })

  it('should display an hint when no product search by id was found', () => {
    productFocusPage.navigateTo('120')
    expect(browser.getCurrentUrl()).toEqual(browser.baseUrl + 'products/120')
    expect(browser.isElementPresent(productFocusPage.getProductNotFoundMessage())).toBeTruthy()
    browser.manage().logs().get(logging.Type.BROWSER).catch()
  })

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER)
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE
    } as logging.Entry))
  })
})
